﻿// © Kotyarko_O, 2020 \\

 #ifndef UNICODE
  #error Just Unicode!
#endif
 #ifndef IS_ENHANCED
  #error You must have the enhanced revision of Inno Setup Compiler to build this project
#endif

#define Author "Kotyarko_O"

#define AppID "{Kotyarko-4609-4DAD-A2D4-06D4086BD394}"
#define AppMutex "KMPMutex"
#define AppFullName "Kotyarko_O`s ModPack"
#define AppShortName "KMP"
#define GameFullName "World of Tanks"
#define GameShortName "WoT"

#define CreateDate GetDateTimeString('dd/mm/yyyy hh:nn:ss', '.', ':')

#define URL_KoreanRandom "https://koreanrandom.com/forum/"
#define URL_XVMSite "https://modxvm.com/"
#define URL_WOTForum "http://forum.worldoftanks.ru/topic/1429925-/"
#define URL_Wotsite "https://wotsite.net/gotovye-sborki-modov/12465-"

#define UpdatesURL "https://bitbucket.org/JonykMD/serverfile/downloads/"
#define AnalyticsTID "UA-68124241-4"

//#define UpdatesURL "https://bitbucket.org/Kotyarko_O/kmp/downloads/"