**Обсуждение: #

**Дополнительные ресурсы:**

* [Apache License](http://www.apache.org/licenses/)
* [jrsoftware.org](http://jrsoftware.org/)
* [restools](http://restools.hanzify.org/)
* [InnoSetup Extensions](https://bitbucket.org/XVM/innosetup.extensions)
* [Inno Download Plugin](https://bitbucket.org/mitrich_k/inno-download-plugin)
* [VCL-Styles](https://github.com/RRUZ/vcl-styles-plugins)
* [Bass](http://www.un4seen.com/)
* [7z](https://www.7-zip.org/)
* [iCatalyst](https://github.com/lorents17/iCatalyst)

**Автор: Jonyk.**
**Copyright 2015-2020 The Apache Software Foundation.**
