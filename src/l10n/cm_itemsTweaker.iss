﻿[CustomMessages]
en.itemTweaker=Wot Tweaker:
ru.itemTweaker=Wot Tweaker:

en.itemTweakerEmblems=Remove on-tank emblems.
ru.itemTweakerEmblems=Отключить загрузку эмблем.
en.itemTweakerFog=Disable fog.
ru.itemTweakerFog=Убрать туман (дымку) на картах.
en.itemTweakerDestroyedSmoke=Disable destroyed tanks smoke.
ru.itemTweakerDestroyedSmoke=Убрать дым от уничтоженной техники.
en.itemTweakerShootSmoke=Disable after-shot flame and smoke.
ru.itemTweakerShootSmoke=Убрать дым и пламя при выстреле.
en.itemTweakerExhaustSmoke=Disable exhaust smoke.
ru.itemTweakerExhaustSmoke=Убрать дым из выхлопной трубы техники.
en.itemTweakerSkybox=Disable sky.
ru.itemTweakerSkybox=Убрать облака.
en.itemTweakerTrees=Disable trees moving.
ru.itemTweakerTrees=Убрать движение деревьев.
en.itemTweakerShells=Disable shells explosion effects.
ru.itemTweakerShells=Убрать эффект взрыва снарядов.
en.itemTweakerTankHit=Disable tank hit effects.
ru.itemTweakerTankHit=Убрать эффект попадания в танк.

//DESC

en.descTweaker=
ru.descTweaker=Способен поднять FPS, посредством отключения некоторых эффектов, влияющих на быстродействие и производительность игры. Автор: AtotIK.

en.descTweakerEmblems=
ru.descTweakerEmblems=Отключение загрузки клановых эмблем на технику. Повышает быстродействие игры. Автор: StranikS_Scan.
en.descTweakerFog=
ru.descTweakerFog=Убирает туман с карт и за её пределами.
en.descTweakerDestroyedSmoke=
ru.descTweakerDestroyedSmoke=Убирает дым от уничтоженной техники на поле боя.
en.descTweakerShootSmoke=
ru.descTweakerShootSmoke=Убирает эффекты при любом выстреле, дым и пламя.
en.descTweakerExhaustSmoke=
ru.descTweakerExhaustSmoke=Убирает дым из выхлопной трубы всей техники.
en.descTweakerSkybox=
ru.descTweakerSkybox=Убирает облака (скайбоксы).
en.descTweakerTrees=
ru.descTweakerTrees=Убирает движение деревьев, эффекты проявления погоды.
en.descTweakerShells=
ru.descTweakerShells=Убирает эффекты взрыва снарядов.
en.descTweakerTankHit=
ru.descTweakerTankHit=Убирает эффекты попадания в танк.
