﻿[CustomMessages]
en.itemBaseContours=Enemies edge colors:
ru.itemBaseContours=Контуры обведения противников:
en.itemBaseContoursWhite=white.
ru.itemBaseContoursWhite=белый.
en.itemBaseContoursYellow=yellow.
ru.itemBaseContoursYellow=жёлтый.
en.itemBaseContoursBlue=blue.
ru.itemBaseContoursBlue=синий.

en.itemBaseHangar=Hangar modifications:
ru.itemBaseHangar=Ангарные улучшения:
en.itemBaseHangarFM=In-hangar radio (Wargaming FM).
ru.itemBaseHangarFM=Радио в ангаре (Wargaming FM).
en.itemBaseHangarManager=«Hangar manager».
ru.itemBaseHangarManager=«Менеджер ангаров».
en.itemBaseHangarReplays=«Replays manager».
ru.itemBaseHangarReplays=«Менеджер реплеев».
en.itemBaseHangarSkills=Crew skills detailed descriptions.
ru.itemBaseHangarSkills=Подробное описание умений и навыков экипажа.
en.itemBaseHangarVertTree=Vertical tech tree.
ru.itemBaseHangarVertTree=Вертикальное дерево исследования.
en.itemBaseHangarPremIcons=Gold icons for premium tanks in carousel.
ru.itemBaseHangarPremIcons=Золотые иконки премиум-танков в карусели.

en.itemBaseMCT=Crash tank textures (MCTCreator):
ru.itemBaseMCT=Изменение игровых текстур (MCTCreator):
en.itemBaseMCTTanks=destroyed tanks textures.
ru.itemBaseMCTTanks=изменение текстур уничтоженной техники.
en.itemBaseMCTTracks=tracks textures.
ru.itemBaseMCTTracks=изменение текстур сбитых гусениц\траков.

en.itemBaseExt=Extended modifications:
ru.itemBaseExt=Дополнительные модификации:
en.itemBaseExtBA=Battle Assistant
ru.itemBaseExtBA=Battle Assistant - «САУ здорового человека».
en.itemBaseExtFCPanel=Frag corellation panel minimized.
ru.itemBaseExtFCPanel=Минимизированная панель счёта в бою.
en.itemBaseExtSTurret=Server turret extended.
ru.itemBaseExtSTurret=«Стволик хаоса».
en.itemBaseExtPaintball=Paintball.
ru.itemBaseExtPaintball=Пейнтбол-мод.
en.itemBaseExtKillog=Modified chat and killog icons.
ru.itemBaseExtKillog=Модифицированные иконки чата и киллога.
en.itemBaseExtAntitoxicity=«Antitoxicity».
ru.itemBaseExtAntitoxicity=«Антитоксичность» - фильтр спама в боевом чате.
en.itemBaseExtCamo=Random camouflages.
ru.itemBaseExtCamo=Случайные камуфляжи.

//DESC

en.descBaseContours=Section with modificated enemys's tank edge. Improve in-sight tank visibility on relief. Authors: GPCracker, Kotyarko_O.
ru.descBaseContours=Раздел с модифицированными контурами обведения вражеского танка, для улучшения видимости краёв контура. Авторы: GPCracker, Kotyarko_O.
en.descBaseContoursWhite=White enemy's contour color.
ru.descBaseContoursWhite=Белый контур обведения вражеского танка.
en.descBaseContoursYellow=Yellow enemy's contour color.
ru.descBaseContoursYellow=Жёлтый контур обведения вражеского танка.
en.descBaseContoursBlue=Blue enemy's contour color.
ru.descBaseContoursBlue=Синий контур обведения вражеского танка.

en.descBaseHangar=Section with hangar interface modifications.
ru.descBaseHangar=Раздел с модификациями ангарного интерфейса.
en.descBaseHangarFM=In-hangar WargamingFM radio. Listening also available in battle. Author: Finister.
ru.descBaseHangarFM=Даёт возможность прослушивать радио (Wargaming FM) прямо в игре (как в ангаре, так и во время боя). Автор: Finister.
en.descBaseHangarManager=Simple hangar changing, no need to restart game client. Authors: ShadowHunterRUS, Kotyarko_O.
ru.descBaseHangarManager=Быстрое переключение ангаров, не выходя из игры. Авторы: ShadowHunterRUS, Kotyarko_O.
en.descBaseHangarReplays=Mod allows you to manage your game replays directly from the the game. Authors: STL1te, POL1ROID.
ru.descBaseHangarReplays=Даёт возможность просматривать реплеи, результаты прошлых проведнных боёв, не выходя из игры. Авторы: STL1te, POL1ROID.
en.descBaseHangarSkills=Shows much informatively description about crew skills. Based on Polyacov_Yury's "ButtonReplacer"-modification.
ru.descBaseHangarSkills=Выводит более подробную информацию об умениях и навыках экипажа. На основе мода замены надписей от Polyacov_Yury.
en.descBaseHangarVertTree=Old vertical tech-research tree view. Author: DJON_999.
ru.descBaseHangarVertTree=Мод подойдёт для тех игроков, которые привыкли к старому, вертикальному виду деревьев исследований. Автор: DJON_999.
en.descBaseHangarPremIcons=Marks premium tanks in player's tanks carousel, tech-research tree and achievements view. Author: Kotyarko_O.
ru.descBaseHangarPremIcons=Выделяет премиумную и акционную технику в карусели танков игрока, дереве исследований и достижениях. Автор: Kotyarko_O.

en.descBaseMCT=Change colors for some game textures. Author: StranikS_Scan.
ru.descBaseMCT=Изменение цветов некоторых игровых текстур. Автор: StranikS_Scan.
en.descBaseMCTTanks=Monochromatic crash tank textures (white).
ru.descBaseMCTTanks=Однотонные шкурки для уничтоженных танков игроков - позволяют легко выцеливать прячущегося за ними противника. Элементы и геометрия танка противника, находящегося за укрытием, хорошо различимы.
en.descBaseMCTTracks=Monochromatic crash tank's chassis textures (white).
ru.descBaseMCTTracks=Однотонные текстуры сбитых траков - траки меняют свой цвет каждый раз, когда живому танку сбивают гусеницу или когда танк уничтожается.

en.descBaseExt=Some extended modifications.
ru.descBaseExt=Раздел с дополнительными модификациями, не попавшими в разделы выше.
en.descBaseExtBA=Changes the way SPG aims at target. The camera follows the trajectory of the shell. The new vanilla arty mode was made as an effort to copy the original Battle Assistant mod (this mod). The copy obviously is not that good as the original Battle Assistant. Author: reven86.
ru.descBaseExtBA=Изменяет вид прицеливания на САУ, делает прицеливание более схожим со снайперским режимом на других видах техники. Для перехода в новый режим нужно в артиллерийском режиме нажать "J" или среднюю кнопку мыши. Автор: reven86.
en.descBaseExtFCPanel=Minimize "frag corellation" panel. Author: Kotyarko_O.
ru.descBaseExtFCPanel=Делает панель счёта в бою минимизированной и не броской. Автор: Kotyarko_O.
en.descBaseExtSTurret=Modification combinates server sight and player's tank turret-rotator. Author: spoter.
ru.descBaseExtSTurret=Мод совмещает серверный прицел с моделью башни и орудия вашего танка. Они будут двигаться согласно данным серверного прицела. С возможностью детальной настройки в ангаре. Автор: spoter.
en.descBaseExtPaintball=More visible on-tank hits and detecting type of it (penetrate, ricochete and etc.). Author: vlad_cs_sr.
ru.descBaseExtPaintball=Делает более заметные попадания и их тип (пробитие, рикошет и т.д.). Автор: vlad_cs_sr.
en.descBaseExtKillog=More visible killog events and battle chat commands. Based on Polyacov_Yury's "ButtonReplacer"-modification. Author: NooBooL.
ru.descBaseExtKillog=Делает более заметными события в кил-логе и боевые команды в чате. Автор конфигурации: NooBooL. На основе мода замены надписей от Polyacov_Yury.
en.descBaseExtAntitoxicity=Modification prevents minimap clicking spam, and battle chat commands spam. Limits messages count by one player. Author: ShuraBB.
ru.descBaseExtAntitoxicity=Мод настроен на борьбу со спамом кликов по координатам миникарты, в боевом чате. Ограничивает число выводимых сообщений от одного игрока. Автор: ShuraBB.
en.descBaseExtCamo=Camouflages added by this modification visible only for player. All camouflages is picked randomly, except for tanks which already has own bought camouflage. Authors: tratatank, Polyacov_Yury.
ru.descBaseExtCamo=Камуфляжи, добавленные этим модом, видны только игроку. Все камуфляжи подбираются в случайном порядке, за исключением танков, на которых установлен купленный камуфляж. Авторы: tratatank, Polyacov_Yury.
