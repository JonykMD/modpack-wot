﻿// © Kotyarko_O, 2020 \\

[CustomMessages]
en.languageIsNotFullySupports=Setup English translation is not full enough yet. Some of items descriptions may be not translated.%nWe working on it, sorry for the inconvenience ;)
ru.languageIsNotFullySupports=

en.warning=Warning!
ru.warning=Внимание!

en.runningApplicationFound=Running "{#GameFullName}" application found.%nIt is recommended that you allow Setup to automatically close these application.
ru.runningApplicationFound=Обнаружено запущенное приложение "{#GameFullName}".%nПеред продолжением требуется закрыть все экземпляры приложения.%nЗакрыть игру?

en.updCheckButtonText=Check...
ru.updCheckButtonText=Проверка...
en.updDownloadButtonText=Download...
ru.updDownloadButtonText=Загрузка...
en.updStatusLabelText=Update status:
ru.updStatusLabelText=Состояние обновления:
en.updInfoLabelTextDefault=Check for updates is not performed.
ru.updInfoLabelTextDefault=Проверка не произведена.
en.updInfoLabelServerFailText=Server connection failed or there is no internet connection at all.%nClick «Next» to continue installation.
ru.updInfoLabelServerFailText=Подключение к серверу не удалось или отсутствует подключение к сети.%nВы можете продолжить установку в обычном режиме, нажав кнопку «Далее».
en.updInfoLabelVersionFailText=Unknown version.%nClick «Check» to retry. Or click «Next» to continue installation.
ru.updInfoLabelVersionFailText=Неизвестная версия. Чтобы проверить обновления снова, нажмите кнопку «Проверка».%nИли продолжайте установку в обычном режиме, нажав кнопку «Далее».
en.updTextLabelDefault=Changes list:
ru.updTextLabelDefault=Список изменений:
en.updRTFPrevChangesText=%n%nPrevious versions changes list:%n%n
ru.updRTFPrevChangesText=%n%nИзменения прошлых версий:%n%n
en.updInfoLabelTextVersionAvailable=There is update available. Your version: {#Version}, new version: %s.%nClick «Download» to automatically update your version.
ru.updInfoLabelTextVersionAvailable=Доступно обновление для инсталлятора. Текущая версия: {#Version}, новая версия: %s.%nДля загрузки новой версии нажмите кнопку «Загрузка».
en.updTextLabelVersionAvailable=Changes in new modpack version:
ru.updTextLabelVersionAvailable=Список изменений в новой версии сборки:
en.updInfoLabelTextVersionBeta=You are using modpack test-version.%nYour version: {#Version}, latest knowing version: %s.
ru.updInfoLabelTextVersionBeta=Вы используете тестовую версию сборки, так как она не указана на сервере.%nВаша версия: {#Version}, версия на сервере: %s.
en.updTextLabelVersionBeta=Details:
ru.updTextLabelVersionBeta=Подробнее:
en.updInfoLabelVersionActual=Version is up to date. You are using actual version ({#Version}).%nClick «Next» to continue.
ru.updInfoLabelVersionActual=Обновление не требуется. Вы используете последнюю версию сборки ({#Version}).%nНажмите кнопку «Далее» для продолжения установки.

en.applicationNotFound=Required "{#GameFullName}" application files not found in directory chosen.
ru.applicationNotFound=Необходимые файлы приложения "{#GameFullName}" не найдены.
en.applicationWrongDir=It is wrong game client root directory. Please, make sure that you choose right game client root directory and try again.
ru.applicationWrongDir=Выбрана неправильная директория для установки.%nПожалуйста, проверьте правильность пути к папке с игрой и попробуйте снова.
en.applicationIncompleteType=It is required to finish installing\updating game client, before continue this Setup.
ru.applicationIncompleteType=Необходимо завершить установку\обновление клиента, прежде чем продолжить установку.
en.applicationPatchUnapplyable=Inappropriate game client version: %s%nThis Setup requires version {#Patch}
ru.applicationPatchUnapplyable=Неподходящая версия установленного клиента: %s%nСборка предназначена для патча {#Patch}

en.googleAnalyticsApplying=I agree for this modpack to use Google Analytics service.
ru.googleAnalyticsApplying=Я согласен на использование этим модпаком сервиса Google Analytics.
en.googleAnalyticsDescription=What is it? Just for author to make sure that modpack have some audience.%nCollected information contains only your game nickname and client region.%nAny modpack user can request for access to view collected information.
ru.googleAnalyticsDescription=Зачем это надо? Только для оценки масштаба аудитории пользователей модпака.%nСобираемые данные содержат только ваш игровой никнейм и регион клиента игры.%nЛюбой пользователь имеет право запросить доступ к просмотру собранных данных.

en.backupHeaderText=To avoid any problems and conflicts from various versions, it is recommend to delete all installed modifications.
ru.backupHeaderText=Для избежания проблем совместимости и конфликтов версий, рекомендуется выбрать вариант очистки папки с модами.
en.deleteButtonText=Delete all installed modifications.
ru.deleteButtonText=Полная очистка игрового клиента от установленных модификаций.
en.backupButtonText=Create already installed modifications backup.
ru.backupButtonText=Создание резервной копии установленных в данный момент модификаций.
en.noneButtonText=Do not do anything. Save already installed modifications (not recommended).
ru.noneButtonText=Не предпринимать никаких действий по отношению к установленным модификациям.
en.cleanProfileButtonText=Clear game client profile (reset client settings and caches).
ru.cleanProfileButtonText=Очистка профиля игрового клиента (сброс индивидуальных настроек и кэша).
en.paramsRememberButtonText=Remember this Setup settings (recommended).
ru.paramsRememberButtonText=Запомнить выбранные параметры текущей установки (рекомендуется).

en.descriptionMemoDefaultText=Move cursor over some item in the list to see it description and preview.
ru.descriptionMemoDefaultText=Наведите курсор на компонент в списке, чтобы увидеть его описание и скриншот.
en.descriptionMemoItemUnavailable= Temporary unavailable.
ru.descriptionMemoItemUnavailable= Временно недоступно.

en.soundPreview=Sound preview
ru.soundPreview=Предпрослушивание звуков
en.soundPreviewVolumeWarning=In avoiding from unexpectedly loud sound playing, check your system volume level.%nAlso take notice that Setup has own volume level (in bottom window part).
ru.soundPreviewVolumeWarning=Во избежание неожиданно громкого звучания, обратите внимание на выставленную громкость в системе.%nТакже, обратите внимание на ползунок громкости в нижней части окна установщика.
en.soundPreviewVolume=Volume: %d%%
ru.soundPreviewVolume=Громкость: %d%%
                                     
en.readyLabelText=Click «Install» to start installation, or click «Back» if you want to review or change any settings.
ru.readyLabelText=Нажмите «Установить», чтобы продолжить, или «Назад», чтобы изменить опции установки.
en.readyMemoExtFunctions=Game client operations:
ru.readyMemoExtFunctions=Функции работы с клиентом:
en.readyMemoExtOptions=Additional Setup tasks:
ru.readyMemoExtOptions=Дополнительные опции установки:
en.readyMemoInstallDir=Install directory:
ru.readyMemoInstallDir=Директория установки:
en.readyMemoSelectedItems=Selected items:
ru.readyMemoSelectedItems=Выбранные компоненты:

en.xvmDownloading=XVM download
ru.xvmDownloading=Загрузка XVM
en.xvmCheckVersionFail=Something went wrong while getting XVM current version information.%nYou can install XVM by yourself. Or restart Setup and try again.
ru.xvmCheckVersionFail=Не удалось получить информацию об актуальных версиях мода XVM.%nВы можете установить мод вручную, загрузив его с официального сайта. Или попробовать перезапустить процесс установки.
en.xvmDownloadingFail=An unexpected error occured while downloading XVM.%nYou can install XVM by yourself. Or restart Setup and try again.
ru.xvmDownloadingFail=При загрузке XVM возникла непредвиденная ошибка.%nВы можете установить мод вручную, загрузив его с официального сайта. Или попробовать перезапустить процесс установки.

en.instStatusLabelTextDeleting=Deleting existing modifications...
ru.instStatusLabelTextDeleting=Удаление ранее установленных модификаций...
en.instStatusLabelTextBackupping=Creating installed modifications backup...
ru.instStatusLabelTextBackupping=Создание резервной копии ранее установленных модификаций...
en.instStatusLabelTextProfileClearing=Clearing game client profile...
ru.instStatusLabelTextProfileClearing=Очистка профайла игрового клиента...

en.instLabelText=Please wait while Setup installs...
ru.instLabelText=Пожалуйста, дождитесь завершения установки...
en.instProgressLabelText=Progress: %.1d%%
ru.instProgressLabelText=Выполнено: %.1d%%

en.instStatusBase=Unpacking base files...
ru.instStatusBase=Распаковка файлов...
en.instStatusXvm=Installing XVM-config...
ru.instStatusXvm=Установка XVM-конфига...
en.instStatusPmod=Installing PMOD...
ru.instStatusPmod=Установка PMOD...
en.instStatusTweaker=Installing Wot-Tweaker...
ru.instStatusTweaker=Установка Wot-Tweaker...

en.finishedPageTasksText=Extended Setup tasks:
ru.finishedPageTasksText=Дополнительные задачи установщика:

en.createUninstallIconButtonText=Create modpack uninstall shortcut icon on desktop.
ru.createUninstallIconButtonText=Создать ярлык на рабочем столе для быстрой деинсталляции сборки.
en.launchGameButtonText=Launch game client (WorldOfTanks.exe) after Setup finished.
ru.launchGameButtonText=Запустить игру (WorldOfTanks.exe) после закрытия установщика.
