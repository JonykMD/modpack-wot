﻿#define VCL "Windows10Dark"

[Files]
Source: "src\vcl\VCLStylesInno.dll"; Flags: dontcopy
Source: "src\vcl\{#VCL}.vsf"; Flags: dontcopy

[Code]
Procedure LoadVCLStyle(VClStyleFile: String); external 'LoadVCLStyleW@files:VCLStylesInno.dll stdcall';
Procedure UnLoadVCLStyles(); external 'UnLoadVCLStyles@files:VCLStylesInno.dll stdcall';

Procedure InitializeVCL();
begin
 ExtractTemporaryFile('{#VCL}.vsf');
 LoadVCLStyle(ExpandConstant('{tmp}\{#VCL}.vsf'));
end;
