﻿// © Kotyarko_O, 2020 \\

//#define GA_DEBUG_MODE

[Files]
Source: "src\analytics\GoogleAnalytics.dll"; Flags: dontcopy nocompression;

[Code]
Const
 GA_DEBUG_MODE = {#ifdef GA_DEBUG_MODE}True{#else}False{#endif};

 //Events
 GA_E_START = 'start';
 GA_E_KEEP = 'keep';
 GA_E_FINISH = 'end';

Type
 TGoogleAnalytics = record
  Timer: TTimer;
  Error: Boolean;
  Exception: String;
 end;

Var
 GoogleAnalytics: TGoogleAnalytics;

// Available macros in "Params" arg:
//  {OSIDMD5}  - tries to replace '{OSIDMD5}'-string into MD5 hash of CurrentVersion\ProductId from registry.
Function PostGAnalytics(const AParams: String; ADebug: Boolean; var ResponseCode: Integer): Boolean; external 'PostGAnalytics@files:GoogleAnalytics.dll stdcall';

Procedure AnalyticsAction(const Params: String);
var
 ResponseCode: Integer;
begin
 if GoogleAnalytics.Error then
  Exit;

 try
  if not PostGAnalytics(
   'v=1' +                    //protocol version
   '&tid={#AnalyticsTID}' +   //tracking identifier
   '&ds=app' +                //data source
   '&cid={OSIDMD5}' +         //client identifier
   '&ul=ru' +                 //user language
   '&an={#AppFullName}' +     //application name
   '&av={#Version}' +         //application version
   Params
  , GA_DEBUG_MODE, ResponseCode) then
  begin
   //Only for Debug. Lib shows own message for this case.
   //MsgBoxEx(0, 'Response code: ' + IntToStr(ResponseCode), 'GoogleAnalytics', MB_ICONWARNING or MB_OK, 0, 0);
   GoogleAnalytics.Error := True;
  end;
 except
  GoogleAnalytics.Error := True;
  GoogleAnalytics.Exception := GetExceptionMessage();
 end;

 if GoogleAnalytics.Error and Assigned(GoogleAnalytics.Timer) then
 begin
  GoogleAnalytics.Timer.Enabled := False;
  GoogleAnalytics.Timer.Free();
  MsgBoxEx(0, GoogleAnalytics.Exception, 'GoogleAnalytics', MB_ICONWARNING or MB_OK, 0, 0);
 end;
end;

Procedure AnalyticsSessionCtrl(const Event: String);
var
 SC: String;
begin
 case Event of
  GA_E_START, GA_E_FINISH: SC := '&sc=' + Event; //seance controller
 end;
 AnalyticsAction(
   SC +
   '&t=event' +               //type
   '&ec=session' +            //event category
   '&ea=' + Event             //event action
 );
end;

Procedure GoogleAnalyticsOnTimer(Sender: TObject);
begin
 AnalyticsSessionCtrl(GA_E_KEEP);
end;

//Published

Procedure AnalyticsSessionStart();
begin
 AnalyticsSessionCtrl(GA_E_START);

 if GoogleAnalytics.Error then
  Exit;

 GoogleAnalytics.Timer := TTimer.Create(WizardForm);
 with GoogleAnalytics.Timer do
 begin
  Enabled := True;
  Interval := 10000;
  OnTimer := @GoogleAnalyticsOnTimer;
 end;
end;

Procedure AnalyticsSessionFinish();
begin
 if GoogleAnalytics.Error then
  Exit;

 if Assigned(GoogleAnalytics.Timer) then
  GoogleAnalytics.Timer.Free();

 AnalyticsSessionCtrl(GA_E_FINISH);
end;
