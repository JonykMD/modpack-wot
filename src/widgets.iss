﻿// © Kotyarko_O, 2020 \\

[Code]
Var
 KRLogoBtn, XVMLogoBtn, WOTLogoBtn, WotsiteLogoBtn: Longint;

Procedure WidgetsOnClick(hBtn: Longint);
var
 ErrorCode: Integer;
begin
 case hBtn of
  KRLogoBtn: ShellExec('', '{#URL_KoreanRandom}', '', '', SW_SHOW, ewNoWait, ErrorCode);
  XVMLogoBtn: ShellExec('', '{#URL_XVMSite}', '', '', SW_SHOW, ewNoWait, ErrorCode);
  WOTLogoBtn: ShellExec('', '{#URL_WOTForum}', '', '', SW_SHOW, ewNoWait, ErrorCode);
  WotsiteLogoBtn: ShellExec('', '{#URL_Wotsite}', '', '', SW_SHOW, ewNoWait, ErrorCode);
 end;
end;

Procedure InitializeWidgets();
begin
 KRLogoBtn := BtnCreate(WizardForm.Handle, ScaleX(8), WizardForm.ClientHeight - ScaleY(44), 40, 40, 'logoBtnKr.png', 0, False);
 BtnSetCursor(KRLogoBtn, GetSysCursorHandle(OCR_HAND));
 BtnSetEvent(KRLogoBtn, BtnClickEventID, WrapBtnCallback(@WidgetsOnClick, 1));

 XVMLogoBtn := BtnCreate(WizardForm.Handle, ScaleX(60), WizardForm.ClientHeight - ScaleY(41), 34, 34, 'logoBtnXvm.png', 0, False);
 BtnSetCursor(XVMLogoBtn, GetSysCursorHandle(OCR_HAND));
 BtnSetEvent(XVMLogoBtn, BtnClickEventID, WrapBtnCallback(@WidgetsOnClick, 1));

 WOTLogoBtn := BtnCreate(WizardForm.Handle, ScaleX(104), WizardForm.ClientHeight - ScaleY(44), 40, 40, 'logoBtnWot.png', 0, False);
 BtnSetCursor(WOTLogoBtn, GetSysCursorHandle(OCR_HAND));
 BtnSetEvent(WOTLogoBtn, BtnClickEventID, WrapBtnCallback(@WidgetsOnClick, 1));

 WotsiteLogoBtn := BtnCreate(WizardForm.Handle, ScaleX(154), WizardForm.ClientHeight - ScaleY(44), 40, 40, 'logoBtnWotsite.png', 0, False);
 BtnSetCursor(WotsiteLogoBtn, GetSysCursorHandle(OCR_HAND));
 BtnSetEvent(WotsiteLogoBtn, BtnClickEventID, WrapBtnCallback(@WidgetsOnClick, 1));
end;