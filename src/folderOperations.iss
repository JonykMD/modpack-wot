﻿// © Kotyarko_O, 2020 \\

[Code]
Procedure ClientFolderOperations(CurStep: TSetupStep);
var
 ResultCode: Integer;
 WoTAppData, WoTAppDataBackup: String;
begin
 if CurStep = ssInstall then
 try
  if CheckBoxGetChecked(RBDelete) then
  begin
   InstStatusLabel.Caption := CustomMessage('instStatusLabelTextDeleting');
   InstFileNameLabel.Caption := ExpandConstant('{app}\mods|res_mods\*');

   Exec(ExpandConstant('{cmd}'), '/C RMDIR /S /Q "res_mods"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   Exec(ExpandConstant('{cmd}'), '/C RMDIR /S /Q "mods"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   ForceDirectories(ExpandConstant('{app}\res_mods\{#Patch}'));
   ForceDirectories(ExpandConstant('{app}\mods\{#Patch}'));
  end;
  if CheckBoxGetChecked(RBBackup) then
  begin
   InstStatusLabel.Caption := CustomMessage('instStatusLabelTextBackupping');
   InstFileNameLabel.Caption := ExpandConstant('{app}\mods|res_mods\*');

   if DirExists(ExpandConstant('{app}\backup_{#Patch}')) then
    Exec(ExpandConstant('{cmd}'), '/C RMDIR /S /Q "backup_{#Patch}"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);

   ForceDirectories(ExpandConstant('{app}\backup_{#Patch}'));
   Exec(ExpandConstant('{cmd}'), '/C MOVE /Y "res_mods" "backup_{#Patch}"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   Exec(ExpandConstant('{cmd}'), '/C MOVE /Y "mods" "backup_{#Patch}"', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);

   ForceDirectories(ExpandConstant('{app}\res_mods\{#Patch}'));
   ForceDirectories(ExpandConstant('{app}\mods\{#Patch}'));
  end;
  if CheckBoxGetChecked(CBCleanProfile) then
  begin
   WoTAppData := ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks\');
   WoTAppDataBackup := ExpandConstant('{userappdata}\Wargaming.net\WorldOfTanks_backup');

   InstStatusLabel.Caption := CustomMessage('instStatusLabelTextProfileClearing');
   InstFileNameLabel.Caption := WoTAppData;

   if DirExists(WoTAppDataBackup) then
    DelTree(WoTAppDataBackup, True, True, True);
   ForceDirectories(WoTAppDataBackup);
   Exec(ExpandConstant('{cmd}'), '/C XCOPY "' + WoTAppData + '*.*" "' + WoTAppDataBackup + '\" /S /F /Y', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);

   DelTree(WoTAppData + 'account_caches', True, True, True);
   DelTree(WoTAppData + 'battle_results', True, True, True);
   DelTree(WoTAppData + 'clan_cache', True, True, True);
   DelTree(WoTAppData + 'custom_data', True, True, True);
   DelTree(WoTAppData + 'dossier_cache', True, True, True);
   DelTree(WoTAppData + 'messenger_cache', True, True, True);
   DelTree(WoTAppData + 'pmod', True, True, True);
   DelTree(WoTAppData + 'profile', True, True, True);
   DelTree(WoTAppData + 'storage_cache', True, True, True);
   DelTree(WoTAppData + 'tutorial_cache', True, True, True);
   DelTree(WoTAppData + 'veh_cmp_cache', True, True, True);
   DelTree(WoTAppData + 'web_cache', True, True, True);
   DelTree(WoTAppData + 'wgfm', True, True, True);
  end;
  SaveStringToFile(ExpandConstant('{app}\res_mods\{#Patch}\readme.txt'), 'This folder is used for World of Tanks modifiers (mods).', False);
  SaveStringToFile(ExpandConstant('{app}\mods\{#Patch}\readme.txt'), 'This folder is used for packaged World of Tanks modifiers (*.wotmods).', False);
 except
  MsgBoxEx(WizardForm.Handle, GetExceptionMessage(), '{#__FILE__}: {#__LINE__}', MB_OK or MB_ICONERROR, 0, 0);
 end;
end;

Procedure RestoreDirectories(CurUninstallStep: TUninstallStep);
var
 ResultCode: Integer;
begin
 if CurUninstallStep = usPostUninstall then
 begin
  if DirExists(ExpandConstant('{app}\backup_{#Patch}')) then
  begin
   DelTree(ExpandConstant('{app}\res_mods'), True, True, True);
   DelTree(ExpandConstant('{app}\mods'), True, True, True);
   
   Exec(ExpandConstant('{cmd}'), '/C MOVE /Y "backup_{#Patch}\mods" ""', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);
   Exec(ExpandConstant('{cmd}'), '/C MOVE /Y "backup_{#Patch}\res_mods" ""', ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ResultCode);

   DelTree(ExpandConstant('{app}\backup_{#Patch}'), True, True, True);
  end;

  if not DirExists(ExpandConstant('{app}\res_mods\{#Patch}')) then
   ForceDirectories(ExpandConstant('{app}\res_mods\{#Patch}'));
  if not DirExists(ExpandConstant('{app}\mods\{#Patch}')) then
   ForceDirectories(ExpandConstant('{app}\mods\{#Patch}'));

  SaveStringToFile(ExpandConstant('{app}\res_mods\{#Patch}\readme.txt'), 'This folder is used for World of Tanks modifiers (mods).', False);
  SaveStringToFile(ExpandConstant('{app}\mods\{#Patch}\readme.txt'), 'This folder is used for packaged World of Tanks modifiers (*.wotmods).', False);
 end;
end;