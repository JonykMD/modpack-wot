﻿// © Kotyarko_O, 2020 \\

[Files]
Source: "src\7za.exe"; Flags: dontcopy nocompression;

[Code]
Var
 XVMDownloadPage: TWizardPage;
 XVMDownloadPageImg, XVMDownloadPageNameImg: Longint;

 IsReleaseAvailable, IsXVMReadyToExtract, IsXVMDataAvailable, IsXVMDataProcessed: Boolean;
 ReleaseVersionURL, NightlyVersionURL, XVMPath: String;

///

Function GetXVMDownloadData(var AIsReleaseAvailable: Boolean; var AReleaseVersionURL, ANightlyVersionURL: String): Boolean;
var
 ServerFile, XVMSec, StrIsReleaseAvailable: String;
begin
 Result := False;
 ServerFile := ExpandConstant('{tmp}\updaterServer.xml');
 if FileExists(ServerFile) then
 begin
  Result := XMLFileReadValue(ServerFile, Format('updaterServer.xml\%s\xvm', [ActiveLanguage()]), XVMSec);
  if Result then
  begin
   XMLStringReadValue(XVMSec, 'isReleaseAvailable', False, StrIsReleaseAvailable);
   AIsReleaseAvailable := StrToBool(StrIsReleaseAvailable) and not CMDCheckParams(CMD_ForceNightlyXVM);
   XMLStringReadValue(XVMSec, 'releaseVersionURL', False, AReleaseVersionURL);
   XMLStringReadValue(XVMSec, 'nightlyVersionURL', False, ANightlyVersionURL);
  end;
 end;
end;

Procedure CheckIsXVMDataAvailable();
begin
 if IsXVMDataProcessed then
  Exit;

 IsXVMDataAvailable := GetXVMDownloadData(IsReleaseAvailable, ReleaseVersionURL, NightlyVersionURL) and ((Length(ReleaseVersionURL) > 1) or (Length(NightlyVersionURL) > 1));

 if not IsXVMDataAvailable then
  MsgBoxEx(WizardForm.Handle, CustomMessage('xvmCheckVersionFail'), CustomMessage('xvmDownloading'), MB_ICONERROR or MB_OK, 0, 0);

 IsXVMDataProcessed := True;
end;

///

Procedure SetXVMDownloadPageVisibility(const Value: Boolean);
begin
 ImgSetVisibility(XVMDownloadPageImg, Value);
 ImgSetVisibility(XVMDownloadPageNameImg, Value);
end;

Function XVMOnDownloadFinished(): Boolean;
begin
 case IsReleaseAvailable of
  True: IsXVMReadyToExtract := (idpFilesCount() > 0) and idpFileDownloaded(ReleaseVersionURL) and FileExists(ExpandConstant('{app}\xvm.zip'));
  False: IsXVMReadyToExtract := (idpFilesCount() > 0) and idpFileDownloaded(NightlyVersionURL) and FileExists(ExpandConstant('{app}\xvm.zip'));
 end;
 if not IsXVMReadyToExtract then
  MsgBoxEx(WizardForm.Handle, CustomMessage('xvmDownloadingFail'), CustomMessage('xvmDownloading'), MB_ICONERROR or MB_OK, 0, 0);

 IDPDeactivate();
 SetXVMDownloadPageVisibility(False);

 Result := True;
end;

Procedure StartXVMExtracting();
var
 ErrorCode: Integer;
begin
 if IsXVMReadyToExtract then
 begin
  ExtractTemporaryFile('7za.exe');
  ShellExec('', ExpandConstant('{tmp}\7za.exe'), Format('x "%s" -o".\" -aoa -y -xr!*.sample -xr!configs.url -xr!readme-*.txt -xr!8.?.?_*', [XVMPath]), ExpandConstant('{app}'), SW_SHOW, ewWaitUntilTerminated, ErrorCode);

  if FileExists(XVMPath) then
   DeleteFile(XVMPath);

  //StartXVMExtracting is calling on "AfterInstall"-event (this event calls on each file that extracting).
  //So we have to disable this flag to prevent multiple extracting attempts.
  IsXVMReadyToExtract := False;
 end;
end;

Function XVMDownloadPageOnShouldSkipPage(Sender: TWizardPage): Boolean;
begin
 if FileExists(ExpandConstant('{src}\xvm.zip')) then
 begin
  XVMPath := ExpandConstant('{src}\xvm.zip');
  IsXVMReadyToExtract := True;
  Result := True;
  Exit;
 end;

 CheckIsXVMDataAvailable();
 Result := not (XVMChecked('itemXvm') and IsXVMDataAvailable);
end;

Procedure XVMDownloadPageOnActivate(Sender: TWizardPage);
begin
 XVMPath := ExpandConstant('{app}\xvm.zip');
 if FileExists(XVMPath) then
  DeleteFile(XVMPath);

 case IsReleaseAvailable of
  True: idpAddFile(ReleaseVersionURL, XVMPath);
  False: idpAddFile(NightlyVersionURL, XVMPath);
 end;

 idpSetInternalOption('allowcontinue', 'yes');
 idpSetInternalOption('connecttimeout', '3000');

 IDPInitializeDownload(XVMDownloadPage, @XVMOnDownloadFinished);

 SetXVMDownloadPageVisibility(True);
end;

Function XVMDownloadPageOnNextButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetXVMDownloadPageVisibility(False);
end;

Procedure InitializeXVMDownloadPage();
begin
 XVMDownloadPage := CreateCustomPage(wpPreparing, '', '');
 with XVMDownloadPage do
 begin
  OnShouldSkipPage := @XVMDownloadPageOnShouldSkipPage;
  OnActivate := @XVMDownloadPageOnActivate;
  OnNextButtonClick := @XVMDownloadPageOnNextButtonClick;
 end;

 XVMDownloadPageImg := ImgLoad(WizardForm.Handle, 'pageXvmDownload.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 XVMDownloadPageNameImg := ImgLoad(WizardForm.Handle, Format('pageXvmDownloadName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 SetXVMDownloadPageVisibility(False);
end;