﻿// © Kotyarko_O, 2020 \\

[Code]
Var
 WelcomePage: TWizardPage;
 WelcomePageImg, WelcomePageNameImg: Longint;

Procedure SetWelcomePageVisibility(Value: Boolean);
begin
 ImgSetVisibility(WelcomePageImg, Value);
 ImgSetVisibility(WelcomePageNameImg, Value);
 WizardForm.BackButton.Visible := not Value;
end;

Procedure WelcomePageOnActivate(Sender: TWizardPage);
begin
 SetWelcomePageVisibility(True);
end;

Function WelcomePageOnNextButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := {#ifdef CheckForGameRun}CheckForGameRun(WizardForm.Handle){#else}True{#endif};
 if Result then
  SetWelcomePageVisibility(False);
end;

Procedure InitializeWelcomePage();
begin
 WelcomePage := CreateCustomPage(wpWelcome, '', '');
 with WelcomePage do
 begin
  OnActivate := @WelcomePageOnActivate;
  OnNextButtonClick := @WelcomePageOnNextButtonClick;
 end;

 WelcomePageImg := ImgLoad(WizardForm.Handle, 'pageWelcome.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 WelcomePageNameImg := ImgLoad(WizardForm.Handle, Format('pageWelcomeName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 SetWelcomePageVisibility(False);
end;