﻿// © Kotyarko_O, 2020 \\

#include "findwot.iss"

//wpSelectDir page is skipping (see below "ShouldSkipPage")
//so, {app} will defined with static path ([Setup]-setting)
//its recommended to use only WizardForm.DirEdit.Text and WizardDirValue() in SelectDirPage

[Code]
Var
 SelectDirPage: TWizardPage;
 DiskSpaceLabel, ClientGALabel, BackupHeaderLabel: TLabel;
 SelectDirPageImg, SelectDirPageNameImg, RBDelete, RBBackup, RBNone, CBCleanProfile, CBParamsRemember, CBClientGA: Longint;

Procedure SetSelectDirPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(SelectDirPageImg, Value);
 ImgSetVisibility(SelectDirPageNameImg, Value);
 DiskSpaceLabel.Visible := Value;
 CheckBoxSetVisibility(CBClientGA, Value);
 ClientGALabel.Visible := Value
 WOTList.Visible := Value;
 DirBrowseButton.Visible := Value;
 BackupHeaderLabel.Visible := Value;
 CheckBoxSetVisibility(RBDelete, Value);
 CheckBoxSetVisibility(RBBackup, Value);
 CheckBoxSetVisibility(RBNone, Value);
 CheckBoxSetVisibility(CBCleanProfile, Value);
 CheckBoxSetVisibility(CBParamsRemember, Value);
end;

Procedure SelectDirPageOnActivate(Sender: TWizardPage);
begin
 InitializeFindWOT();
 SetSelectDirPageVisibility(True);
end;

Function SelectDirPageOnBackButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetSelectDirPageVisibility(False);
end;

Function SelectDirPageOnNextButtonClick(Sender: TWizardPage): Boolean;
 #ifdef CheckForGameFiles
var
 PatchVersion, AppType: String;
#endif
begin
 Result := True;
  #ifdef CheckForGameFiles
 if CMDCheckParams(CMD_NoSearchGameFiles) then
 begin
  SetSelectDirPageVisibility(False);
  Exit;
 end;
 if not FilesExists([WizardDirValue() + '\WorldOfTanks.exe', WizardDirValue() + '\version.xml', WizardDirValue() + '\app_type.xml']) then
 begin
  MsgBoxEx(WizardForm.Handle, CustomMessage('applicationWrongDir'), CustomMessage('warning'), MB_OK or MB_ICONWARNING, 0, 0);
  Result := False;
 end else
 begin
  XMLFileReadValue(WizardDirValue() + '\app_type.xml', 'protocol\app_type', AppType);
  Result := AppType <> 'incomplete';
  if not Result then
   MsgBoxEx(WizardForm.Handle, CustomMessage('applicationIncompleteType'), CustomMessage('warning'), MB_ICONWARNING or MB_OK, 0, 0)
  else
  begin
   XMLFileReadValue(WizardDirValue() + '\version.xml', 'version.xml\version', PatchVersion);
   Delete(PatchVersion, Pos('v', PatchVersion), 2);
   Delete(PatchVersion, Pos('#', PatchVersion) - 1, 10);
   Result := CompareStr(PatchVersion, '{#Patch}') = 0;
   if not Result then
    MsgBoxEx(WizardForm.Handle, Format(CustomMessage('applicationPatchUnapplyable'), [PatchVersion]), CustomMessage('warning'), MB_OK or MB_ICONWARNING, 0, 0);
  end;
 end;
 #endif
 if Result then
  SetSelectDirPageVisibility(False);
end;

Function SelectDirShouldSkipPage(): Boolean;
begin
 Result := True;
end;

Procedure CBsOnClick(hBtn: Longint);
begin
 case hBtn of
  CBClientGA: SetInstallParam('CheckBox_ClientGoogleAnalytics', BoolToStr(CheckBoxGetChecked(CBClientGA)));
 end;
end;

Procedure InitializeSelectDirPage();
begin
 SelectDirPage := CreateCustomPage({#ifdef Updater}UpdaterPage.ID{#else}WelcomePage.ID{#endif}, '', '');
 with SelectDirPage do
 begin
  OnActivate := @SelectDirPageOnActivate;
  OnBackButtonClick := @SelectDirPageOnBackButtonClick;
  OnNextButtonClick := @SelectDirPageOnNextButtonClick;
 end;

 SelectDirPageImg := ImgLoad(WizardForm.Handle, 'pageSelectDir.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 SelectDirPageNameImg := ImgLoad(WizardForm.Handle, Format('pageSelectDirName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 DiskSpaceLabel := TLabel.Create(SelectDirPage);
 with DiskSpaceLabel do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(195), ScaleY(140), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 10;
  Caption := WizardForm.DiskSpaceLabel.Caption;
 end;

//FindWOT
 WOTList := TComboBox.Create(SelectDirPage);
 with WOTList do
 begin
  Parent := WizardForm;
  SetBounds(DiskSpaceLabel.Left, DiskSpaceLabel.Top + DiskSpaceLabel.Height + ScaleY(20), ScaleX(480), WizardForm.DirEdit.Height + ScaleY(5));
  Font.Size := 9;
  OnChange := @WOTListOnChange;
 end;

 DirBrowseButton := TButton.Create(SelectDirPage);
 with DirBrowseButton do
 begin
  Parent := WizardForm;
  SetBounds(WOTList.Left + WOTList.Width + ScaleX(10), WOTList.Top, ScaleX(85), WizardForm.DirBrowseButton.Height);
  Caption := WizardForm.DirBrowseButton.Caption;
  OnClick := @WOTListOnChange;
 end;
//FindWOT

 CBClientGA := CheckBoxCreate(WizardForm.Handle, DiskSpaceLabel.Left, WOTList.Top + WOTList.Height + ScaleY(10), ScaleX(535), ScaleY(20), 'checkBox.png', 0, 2);
  CheckBoxSetText(CBClientGA, CustomMessage('googleAnalyticsApplying'));
  CheckBoxSetFont(CBClientGA, BotvaFont.Handle);
  CheckBoxSetFontColor(CBClientGA, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetEvent(CBClientGA, BtnClickEventID, WrapBtnCallback(@CBsOnClick, 1));
  CheckBoxSetChecked(CBClientGA, GetInstallParam('CheckBox_ClientGoogleAnalytics', True));

 ClientGALabel := TLabel.Create(SelectDirPage);
 with ClientGALabel do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(190), ScaleY(240), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 10;
  Caption := CustomMessage('googleAnalyticsDescription');
 end;

 BackupHeaderLabel := TLabel.Create(SelectDirPage);
 with BackupHeaderLabel do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(18), ScaleY(360), 0, 0);
  AutoSize := True;
  Transparent := True;
  Font.Size := 10;
  Caption := CustomMessage('backupHeaderText');
  Alignment := taCenter;
 end;

 RBDelete := CheckBoxCreate(WizardForm.Handle, ScaleX(22), BackupHeaderLabel.Top + BackupHeaderLabel.Height + ScaleY(5), ScaleX(425), ScaleY(20), 'radioButton.png', 1, 2);
  CheckBoxSetText(RBDelete, CustomMessage('deleteButtonText'));
  CheckBoxSetFont(RBDelete, BotvaFont.Handle);
  CheckBoxSetFontColor(RBDelete, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetChecked(RBDelete, True);
 RBBackup := CheckBoxCreate(WizardForm.Handle, ScaleX(22), BackupHeaderLabel.Top + BackupHeaderLabel.Height + ScaleY(30), ScaleX(475), ScaleY(20), 'radioButton.png', 1, 2);
  CheckBoxSetText(RBBackup, CustomMessage('backupButtonText'));
  CheckBoxSetFont(RBBackup, BotvaFont.Handle);
  CheckBoxSetFontColor(RBBackup, clWhite, $CCCCCC, $D9D9D9, clGray);
 RBNone := CheckBoxCreate(WizardForm.Handle, ScaleX(22), BackupHeaderLabel.Top + BackupHeaderLabel.Height + ScaleY(55), ScaleX(535), ScaleY(20), 'radioButton.png', 1, 2);
  CheckBoxSetText(RBNone, CustomMessage('noneButtonText'));
  CheckBoxSetFont(RBNone, BotvaFont.Handle);
  CheckBoxSetFontColor(RBNone, clWhite, $CCCCCC, $D9D9D9, clGray);
 
 CBCleanProfile := CheckBoxCreate(WizardForm.Handle, ScaleX(22), BackupHeaderLabel.Top + BackupHeaderLabel.Height + ScaleY(85), ScaleX(495), ScaleY(22), 'checkBox.png', 0, 2);
  CheckBoxSetText(CBCleanProfile, CustomMessage('cleanProfileButtonText'));
  CheckBoxSetFont(CBCleanProfile, BotvaFont.Handle);
  CheckBoxSetFontColor(CBCleanProfile, clWhite, $CCCCCC, $D9D9D9, clGray);
 CBParamsRemember := CheckBoxCreate(WizardForm.Handle, ScaleX(22), BackupHeaderLabel.Top + BackupHeaderLabel.Height + ScaleY(110), ScaleX(455), ScaleY(22), 'checkBox.png', 0, 2);
  CheckBoxSetText(CBParamsRemember, CustomMessage('paramsRememberButtonText'));
  CheckBoxSetFont(CBParamsRemember,  BotvaFont.Handle);
  CheckBoxSetFontColor(CBParamsRemember, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetChecked(CBParamsRemember, not FileExists(ExpandConstant(LOG_FILE)));

 SetSelectDirPageVisibility(False);
end;