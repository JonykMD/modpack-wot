﻿// © Kotyarko_O, 2020 \\

[Icons]
Name: "{app}\kmp\{cm:UninstallProgram,{#AppShortName}}"; Filename: "{uninstallexe}"; WorkingDir: "{app}\kmp"; Comment: "{cm:UninstallProgram,{#AppFullName}}."; IconFilename: "{uninstallexe}";

[Code]
Var
 FinishedPage: TWizardPage;
 FinishedPageTasks: TLabel;
 FinishedPageImg, FinishedPageNameImg, CBCreateUninstallIcon, CBGameLaunch: Longint;

Procedure LaunchGame();
var
 ErrorCode: Integer;
begin
 if CheckBoxGetChecked(CBGameLaunch) then
  ShellExec('', 'WorldOfTanks.exe', '', ExpandConstant('{app}'), SW_SHOW, ewNoWait, ErrorCode);
end;

Procedure CBCreateUninstallIconOnClick(hBtn: Longint);
var
 UninsSrtct: String;
begin
 UninsSrtct := ExpandConstant('{userdesktop}\{cm:UninstallProgram,{#AppShortName}}.lnk');
 if CheckBoxGetChecked(hBtn) then
 begin
  if not FileExists(UninsSrtct) then
   FileCopy(ExpandConstant('{app}\kmp\{cm:UninstallProgram,{#AppShortName}}.lnk'), UninsSrtct, False);
 end else
  if FileExists(UninsSrtct) then
   DeleteFile(UninsSrtct);
end;

Procedure SetFinishedPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(FinishedPageImg, Value);
 ImgSetVisibility(FinishedPageNameImg, Value);
 CheckBoxSetVisibility(CBCreateUninstallIcon, Value);
 CheckBoxSetVisibility(CBGameLaunch, Value);
 FinishedPageTasks.Visible := Value;
 if Value then
 begin
  WizardForm.BackButton.Visible := False;
  WizardForm.NextButton.Caption := SetupMessage(msgButtonFinish);
  WizardForm.CancelButton.Visible := False;
 end;
end;

Procedure FinishedPageOnActivate(Sender: TWizardPage);
begin
 SetInstallingPageVisibility(False);
 SetFinishedPageVisibility(True);
end;

Function FinishedPageOnNextButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := False;
 Application.Terminate();
end;

Procedure InitializeFinishedPage();
begin
 FinishedPage := CreateCustomPage(wpInstalling, '', '');
 with FinishedPage do
 begin
  OnActivate := @FinishedPageOnActivate;
  OnNextButtonClick := @FinishedPageOnNextButtonClick;
 end;

 FinishedPageImg := ImgLoad(WizardForm.Handle, 'pageFinished.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 FinishedPageNameImg := ImgLoad(WizardForm.Handle, Format('pageFinishedName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 FinishedPageTasks := TLabel.Create(WizardForm);
 with FinishedPageTasks do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(15), ScaleY(380), 0, 0);
  Font.Size := 11;
  Font.Style := [fsBold];
  Caption := CustomMessage('finishedPageTasksText');
 end;

 CBCreateUninstallIcon := CheckBoxCreate(WizardForm.Handle, ScaleX(22), ScaleY(405), ScaleX(445), ScaleY(22), 'checkBox.png', 0, 2);
  CheckBoxSetText(CBCreateUninstallIcon, CustomMessage('createUninstallIconButtonText'));
  CheckBoxSetFont(CBCreateUninstallIcon, BotvaFont.Handle);
  CheckBoxSetFontColor(CBCreateUninstallIcon, clWhite, $CCCCCC, $D9D9D9, clGray);
  CheckBoxSetEvent(CBCreateUninstallIcon, BtnClickEventID, WrapBtnCallback(@CBCreateUninstallIconOnClick, 1));
 CBGameLaunch := CheckBoxCreate(WizardForm.Handle, ScaleX(22), ScaleY(430), ScaleX(445), ScaleY(22), 'checkBox.png', 0, 2);
  CheckBoxSetText(CBGameLaunch, CustomMessage('launchGameButtonText'));
  CheckBoxSetFont(CBGameLaunch, BotvaFont.Handle);
  CheckBoxSetFontColor(CBGameLaunch, clWhite, $CCCCCC, $D9D9D9, clGray);

 SetFinishedPageVisibility(False);
end;