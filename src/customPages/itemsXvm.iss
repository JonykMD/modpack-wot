﻿// © Kotyarko_O, 2020 \\

#include "..\l10n\cm_itemsXvm.iss"

 #ifndef TESTING
[Files]
Source: "mods\xvm\XVM_KMP_CONFIG\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusXvm'); AfterInstall: StartXVMExtracting(); Check: XVMChecked('itemXvm');

Source: "mods\xvm\tank_icons\betax_colored_clear\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvmBattleIconsBetax1');
Source: "mods\xvm\tank_icons\betax_standard_white\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvmBattleIconsBetax2');
Source: "mods\xvm\tank_icons\xobotyi\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvmBattleIconsXobotyi');
Source: "mods\xvm\tank_icons\blackspy\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvmBattleIconsBlackSpy');
Source: "mods\xvm\tank_icons\seriych_ico_name\*"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res\contour"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvmBattleIconsSeriych');

Source: "mods\xvm\sixth_sense_icons\lol.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvm6SenseLol');
Source: "mods\xvm\sixth_sense_icons\pedobear.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvm6SenseBear');
Source: "mods\xvm\sixth_sense_icons\triangle.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvm6SenseStopSign');
Source: "mods\xvm\sixth_sense_icons\lamp_in_circle.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvm6SenseCircleLamp');
Source: "mods\xvm\sixth_sense_icons\lamp.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvm6SenseSimpleLamp');
Source: "mods\xvm\sixth_sense_icons\eye.png"; DestDir: "{app}\res_mods\mods\shared_resources\xvm\res"; DestName: "SixthSense.png"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusXvm'); Check: XVMChecked('itemXvm6SenseEye');
#endif

[Code]
Var
 XVMPage: TWizardPage;
 XVMList: TNewCheckListBox;
 XVMPageImg, XVMPageNameImg: Longint;

Function XVMChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(XVMList, Name);
end;

Procedure SetXVMPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(XVMPageImg, Value);
 ImgSetVisibility(XVMPageNameImg, Value);
 ResetPreviewImage(Value);
 XVMList.Visible := Value;
 BassVolumeTrackBar.Visible := Value;
 BassVolumeLbl.Visible := Value;
 DescriptionMemo.Visible := Value;
end;

Procedure XVMPageOnActivate(Sender: TWizardPage);
begin
 SetXVMPageVisibility(True);
end;

Function XVMPageOnButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetXVMPageVisibility(False);
end;

Procedure InitializeXVMPage();
begin
 XVMPage := CreateCustomPage(wpSelectComponents, '', '');
 with XVMPage do
 begin
  OnActivate := @XVMPageOnActivate;
  OnBackButtonClick := @XVMPageOnButtonClick;
  OnNextButtonClick := @XVMPageOnButtonClick;
 end;

 XVMPageImg := ImgLoad(WizardForm.Handle, 'pageXvm.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 XVMPageNameImg := ImgLoad(WizardForm.Handle, Format('pageXvmName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 XVMList := TNewCheckListBox.Create(XVMPage);
 with XVMList do
 begin
  Parent := WizardForm;
  SetBounds(ComponentsList.Left, ComponentsList.Top, ComponentsList.Width, ComponentsList.Height);
  Offset := 2;
  Name := 'XVMList';
  Tag := 1;
  Font.Size := ComponentsList.Font.Size;
 end;

 SetCheckListBoxItemsInfo(XVMList.Tag);
 SetCheckListBoxBGBMP(XVMList);
 SetCheckListBoxEvents(XVMList);
 SetXVMPageVisibility(False);

//Items

 AddCheckBoxExt(XVMList, 'itemXvm', 0, True, [fsBold], 'XVM.png');

 AddCheckBoxExt(XVMList, 'itemXvmLogin', 1, True, [fsBold], 'LoginWindow.png');
 AddCheckBoxExt(XVMList, 'itemXvmLoginAuto', 2, True, [], 'LW_AutoLogin.png');
 AddCheckBoxExt(XVMList, 'itemXvmLoginSaveServer', 2, True, [], 'LW_SaveServer.png');

 AddCheckBoxExt(XVMList, 'itemXvmHangar', 1, True, [fsBold], 'HangarInterface.png');
 AddCheckBoxExt(XVMList, 'itemXvmHangarPing', 2, True, [], 'HI_Ping.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarClock', 2, True, [], 'HI_Clock.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarEquip', 2, True, [], 'HI_DeviceReturner.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCrew', 2, True, [], 'HI_CrewReturner.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarousel', 2, True, [fsBold], 'TankCarousel.png');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarouselMastery', 3, True, [], 'TC_MasteryMark.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarouselPercent', 3, True, [], 'TC_WinsPercent.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarouselBattles', 3, True, [], 'TC_Battles.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarouselMarks', 3, True, [], 'TC_MarksOnGun.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarouselDamage', 3, True, [], 'TC_AvgDmg.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmHangarCarouselXte', 3, True, [], 'TC_xTE.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattle', 1, True, [fsBold], 'BattleInterface.png');

 AddCheckBoxExt(XVMList, 'itemXvmBattleCB', 2, True, [fsBold], 'ColorBlind.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattleCBStd', 3, True, [], 'CB_Std.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleCBAlt', 3, True, [], 'CB_Alt.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattleIcons', 2, True, [fsBold], 'TankIcons.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattleIconsBetax1', 3, True, [], 'betax_std_white.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleIconsBetax2', 3, True, [], 'betax_colored_clear.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleIconsXobotyi', 3, True, [], 'xobotyi.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleIconsBlackSpy', 3, True, [], 'BlackSpy.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleIconsSeriych', 3, True, [], 'seriych_name.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattleBLSF', 2, True, [fsBold], 'BattleLoading_StatisticForm.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattleBLSFServices', 3, True, [], 'BLSF_NetworkServices.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleBLSFBattles', 3, True, [], 'BLSF_BattlesCount.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattleBLSFPercent', 3, True, [], 'BLSF_WinsPercent.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattleMM', 2, True, [fsBold], 'Minimap.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMHP', 3, True, [], 'M_HP.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMSquad', 3, True, [], 'M_Squads.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMAllySpotted', 3, True, [], 'M_XMQPSpotted.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMM50m', 3, True, [], 'M_50m.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMM445m', 3, True, [], 'M_445m.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMDynamic', 3, True, [], 'M_DynamicViewRange.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMM564m', 3, True, [], 'M_564m.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMHT10', 3, True, [], 'M_HT10.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMCenter', 3, True, [], 'M_Zoom.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattleMMCenterCtrl', 4, True, [], 'M_Zoom.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattleMMCenterAlt', 4, True, [], 'M_Zoom.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattleMMCenterCaps', 4, True, [], 'M_Zoom.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMAlt', 3, True, [], 'MinimapAlt.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattleMMAltNicks', 4, True, [], 'MAlt_Nicknames.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattlePP', 2, True, [fsBold], 'PlayersPanels.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePPHP', 3, True, [], 'PP_HP.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePPSpotted', 3, True, [], 'SpottedStatus.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePPSpottedStd', 4, True, [], 'SS_Standard.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePPSpottedDot', 4, True, [], 'SS_Dots.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePPSpottedStar', 4, True, [], 'SS_Stars.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePPDead', 3, True, [], 'PP_DeadTanks.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattleVM', 2, True, [fsBold], 'Markers.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMLowHP', 3, True, [], 'LowHPIndicator.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMMastery', 3, True, [], 'MasteryMarksInMarkers.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMAllySpotted', 3, True, [], 'XMQPSpottedInMarkers.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMServices', 3, True, [], 'NetworkServicesInMarkers.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMSquad', 3, True, [], 'SquadInMarkers.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMSquadInStd', 4, True, [], 'SquadInStd.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattleVMSquadInAlt', 4, True, [], 'SquadInAlt.jpg');

 AddCheckBoxExt(XVMList, 'itemXvmBattlePy', 2, True, [fsBold], 'ExtendedSettings.png');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePySphere', 3, True, [], 'SphereDispersion.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyBlocked', 3, True, [], 'BL_Blocked.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyAssist', 3, True, [], 'BL_Assisted.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyRepairTimer', 3, True, [], 'BL_RepairTime.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyRepairControl', 3, True, [], 'BL_RepairControl.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyTeamDamage', 3, True, [], 'TeamDamage.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePySafeShot', 3, True, [], 'SafeShot.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyDamagelog', 3, True, [], 'BL_DamageLog.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyHitlog', 3, True, [fsBold], 'HitLog.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyHitlogSimple', 4, True, [], 'HL_Simple.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyHitlogDetailed', 4, True, [], 'HL_Detailed.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyInfoPanel', 3, True, [fsBold], 'InfoPanels.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyInfoPanelNDO', 4, True, [], 'IP_NDO.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyInfoPanelKMP', 4, True, [], 'IP_KMP.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyInfoPanelFull', 4, True, [], 'IP_StdFull.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyInfoPanelLite', 4, True, [], 'IP_Lite.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyCalc', 3, True, [fsBold], 'BattleEfficiency.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyCalcWN8', 4, True, [], 'BattleEfficiency.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyCalcEFF', 4, True, [], 'BattleEfficiency.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyCalcEFFWN8', 4, True, [], 'BattleEfficiency.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyUGN', 3, True, [fsBold], 'AngleAiming.png');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyUGNCorners', 4, True, [], 'AngleAiming_Legend.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyUGNOctagon', 4, True, [], 'AngleAiming_Legend.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyUGNPrnts', 4, True, [], 'AngleAiming_Legend.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyUGNPrnts2', 4, True, [], 'AngleAiming_Legend.jpg');
 AddCheckBoxExt(XVMList, 'itemXvmBattlePyIAmSpotted', 3, True, [fsBold], 'IAmSpotted.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyIAmSpotted6', 4, True, [], 'IAmSpotted.jpg');
 AddRadioButtonExt(XVMList, 'itemXvmBattlePyIAmSpotted15', 4, True, [], 'IAmSpotted.jpg');

 AddCheckBoxExt(XVMList, 'itemXvm6Sense', 2, True, [fsBold], 'SixthSenseIcons.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseStd', 3, True, [], 'StandardLamp.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseLol', 3, True, [], 'LolLamp.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseBear', 3, True, [], 'PedobearLamp.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseStopSign', 3, True, [], 'StopSignalLamp.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseCircleLamp', 3, True, [], 'Lamp.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseSimpleLamp', 3, True, [], 'SimpleLamp.png');
 AddRadioButtonExt(XVMList, 'itemXvm6SenseEye', 3, True, [], 'EyeLamp.png');
 AddCheckBoxExt(XVMList, 'itemXvm6Sens9Sec', 3, True, [], '9SecLamp.png');

 AddCheckBoxExt(XVMList, 'itemXvmSound6Sense', 2, True, [fsBold], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSound6SenseStd', 3, True, [], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSound6SenseVoice', 3, True, [], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSound6SenseRudy', 3, True, [], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSound6SenseWCountdown', 3, True, [], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSound6SenseWOCountdown', 3, True, [], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSound6SenseEye', 3, True, [], 'Sounds.png');

 AddCheckBoxExt(XVMList, 'itemXvmSoundCrit', 2, True, [fsBold], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSoundCritRing', 3, True, [], 'Sounds.png');
 AddRadioButtonExt(XVMList, 'itemXvmSoundCritRingVoice', 3, True, [], 'Sounds.png');

 AddCheckBoxExt(XVMList, 'itemXvmSoundExt', 2, True, [fsBold], 'Sounds.png');
 AddCheckBoxExt(XVMList, 'itemXvmSoundExtEnemySpotted', 3, True, [], 'Sounds.png');
 AddCheckBoxExt(XVMList, 'itemXvmSoundExtFire', 3, True, [], 'Sounds.png');
 AddCheckBoxExt(XVMList, 'itemXvmSoundExtAmmoBay', 3, True, [], 'Sounds.png');

//SoundItems

 AddItemSound(XVMList, 'itemXvmSound6SenseStd', 'SixthSense_Standard.mp3');
 AddItemSound(XVMList, 'itemXvmSound6SenseVoice', 'SixthSense_Voice.mp3');
 AddItemSound(XVMList, 'itemXvmSound6SenseRudy', 'SixthSense_Rudy.mp3');
 AddItemSound(XVMList, 'itemXvmSound6SenseWCountdown', 'SixthSense_Timer.mp3');
 AddItemSound(XVMList, 'itemXvmSound6SenseWOCountdown', 'SixthSense_NoTimer.mp3');
 AddItemSound(XVMList, 'itemXvmSound6SenseEye', 'SixthSense_Sauron.mp3');

 AddItemSound(XVMList, 'itemXvmSoundCritRing', 'CritDamaged.mp3');
 AddItemSound(XVMList, 'itemXvmSoundCritRingVoice', 'CritDamaged_Voice.mp3');

 AddItemSound(XVMList, 'itemXvmSoundExtEnemySpotted', 'Other_EnemySighted.mp3');
 AddItemSound(XVMList, 'itemXvmSoundExtFire', 'Other_FireAlert.mp3');
 AddItemSound(XVMList, 'itemXvmSoundExtAmmoBay', 'Other_AmmoBay.mp3');

 InstallParams_Mods(CMD_GET, XVMList);
end;

Procedure ConfigurateXVM();
begin
 if not XVMChecked('CheckForChecked') then
  Exit;

//Auto
 ConfigEdit(True, XVM, '@xvm.xc', XVMChecked('itemXvm'), '"url": ""', '"url": "{#URL_WOTForum}"');
 ConfigEdit(True, XVM, '@xvm.xc', XVMChecked('itemXvm'), '"date": ""', '"date": "{#CreateDate}"');
 ConfigEdit(True, XVM, '@xvm.xc', XVMChecked('itemXvm'), '"gameVersion": ""', '"gameVersion": "{#Patch}"');
//Auto
 ConfigEdit(True, XVM, 'login.xc', XVMChecked('itemXvmLoginAuto'), '"autologin": false', '"autologin": true');
 ConfigEdit(True, XVM, 'login.xc', XVMChecked('itemXvmLoginSaveServer'), '"saveLastServer": false', '"saveLastServer": true');
//
 ConfigsEdit(True, XVM, ['login.xc', 'hangar.xc'], XVMChecked('itemXvmHangarPing'), '"enabled": false, //ping', '"enabled": true, //ping');
 ConfigEdit(True, XVM, 'hangar.xc', XVMChecked('itemXvmHangarPing'), '"enabled": true, //serverInfo', '"enabled": false, //serverInfo');
 ConfigEdit(True, XVM, 'widgetsTemplates.xc', XVMChecked('itemXvmHangarClock'), '"enabled": false, //clock', '"enabled": true, //clock');
 ConfigEdit(True, XVM, 'hangar.xc', XVMChecked('itemXvmHangarEquip'), '"enableEquipAutoReturn": false', '"enableEquipAutoReturn": true');
 ConfigEdit(True, XVM, 'hangar.xc', XVMChecked('itemXvmHangarCrew'), '"crewReturnByDefault": false', '"crewReturnByDefault": true');

 ConfigEdit(True, XVM, 'carousel.xc', XVMChecked('itemXvmHangarCarousel'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'carouselNormal.xc', XVMChecked('itemXvmHangarCarouselMastery'), '"enabled": false, //MarkOfMastery', '"enabled": true, //MarkOfMastery');
 ConfigEdit(True, XVM, 'carouselNormal.xc', XVMChecked('itemXvmHangarCarouselPercent'), '"enabled": false, //Winrate', '"enabled": true, //Winrate');
 ConfigEdit(True, XVM, 'carouselNormal.xc', XVMChecked('itemXvmHangarCarouselBattles'), '"enabled": false, //Battles', '"enabled": true, //Battles');
 ConfigEdit(True, XVM, 'carouselNormal.xc', XVMChecked('itemXvmHangarCarouselMarks'), '"enabled": false, //MarksOnGun', '"enabled": true, //MarksOnGun');
 ConfigEdit(True, XVM, 'carouselNormal.xc', XVMChecked('itemXvmHangarCarouselDamage'), '"enabled": false, //Damage', '"enabled": true, //Damage');
 ConfigEdit(True, XVM, 'carouselNormal.xc', XVMChecked('itemXvmHangarCarouselXte'), '"enabled": false, //XTE', '"enabled": true, //XTE');

 ConfigsEdit(True, XVM, ['colors.xc', 'Minimap\minimapLabelsData.xc'], XVMChecked('itemXvmBattleCBStd'), '.ColorNormal', '.ColorBlidnessOLD');
 ConfigsEdit(True, XVM, ['colors.xc', 'Minimap\minimapLabelsData.xc'], XVMChecked('itemXvmBattleCBAlt'), '.ColorNormal', '.ColorBlidnessNEW');
//
 ConfigsEdit(True, XVM, ['battleLoading.xc', 'battleLoadingTips.xc', 'statisticForm.xc'], XVMChecked('itemXvmBattleBLSFServices'), 'NickFormat.Nothing', 'NickFormat.XVMUser');

 ConfigEdit(True, XVM, 'StatisticsTemplates.xc', XVMChecked('itemXvmBattleBLSFPercent'), '{{c:t-battles}}', '{{c:winrate}}');
 ConfigEdit(True, XVM, 'StatisticsTemplates.xc', XVMChecked('itemXvmBattleBLSFPercent'), '{{t-battles%4d|----}}', '{{winrate%d~%|--%}}');
//
 ConfigsEdit(True, XVM, ['battleLoading.xc', 'battleLoadingTips.xc', 'statisticForm.xc', 'playersPanel.xc'], XVMChecked('itemXvmBattleIcons'), '"enabled": false, //TankIcons', '"enabled": true, //TankIcons');
 ConfigEdit(True, XVM, 'playersPanel.xc', XVMChecked('itemXvmBattleIcons'), '"iconAlpha": 90', '"iconAlpha": 0');
 ConfigsEdit(True, XVM, ['battleLoading.xc', 'battleLoadingTips.xc', 'statisticForm.xc'], XVMChecked('itemXvmBattleIcons'), '"vehicleIconAlpha": 100', '"vehicleIconAlpha": 0');
 ConfigsEdit(True, XVM, ['battleLoading.xc', 'battleLoadingTips.xc', 'statisticForm.xc', 'playersPanel.xc'], XVMChecked('itemXvmBattleIconsBetax2') or XVMChecked('itemXvmBattleIconsBlackSpy') or XVMChecked('itemXvmBattleIconsSeriych'), '"scaleX": -1', '"scaleX": 1');
 ConfigsEdit(True, XVM, ['battleLoading.xc', 'battleLoadingTips.xc', 'statisticForm.xc', 'playersPanel.xc'], XVMChecked('itemXvmBattleIconsBetax2') or XVMChecked('itemXvmBattleIconsBlackSpy') or XVMChecked('itemXvmBattleIconsSeriych'), '"x": -95', '"x": 0');

//

 ConfigEdit(True, XVM, 'Minimap\minimap.xc', XVMChecked('itemXvmBattleMM'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'Minimap\minimapLabelsTemplates.xc', XVMChecked('itemXvmBattleMMHP'), '"enabled": false, //HP', '"enabled": true, //HP');
 ConfigEdit(True, XVM, 'Minimap\minimapLabelsTemplates.xc', XVMChecked('itemXvmBattleMMSquad'), '${"SquadIcons.False.', '${"SquadIcons.True.');
 ConfigEdit(True, XVM, 'Minimap\minimapLabelsTemplates.xc', XVMChecked('itemXvmBattleMMAllySpotted'), '"enabled": false, //XMQP', '"enabled": true, //XMQP');
 ConfigEdit(True, XVM, 'Minimap\minimapCircles.xc', XVMChecked('itemXvmBattleMM50m'), '"enabled": false, "distance": 50', '"enabled": true, "distance": 50');
 ConfigEdit(True, XVM, 'Minimap\minimapCircles.xc', XVMChecked('itemXvmBattleMM445m'), '"enabled": false, "distance": 445', '"enabled": true, "distance": 445');
 ConfigEdit(True, XVM, 'Minimap\minimapCircles.xc', XVMChecked('itemXvmBattleMMDynamic'), '"enabled": false, "distance": "dynamic"', '"enabled": true, "distance": "dynamic"');
 ConfigEdit(True, XVM, 'Minimap\minimapCircles.xc', XVMChecked('itemXvmBattleMM564m'), '"enabled": false, "distance": 564', '"enabled": "{{my-vtype-key=SPG?false|true}}", "distance": 564');
 ConfigEdit(True, XVM, 'Minimap\minimapLabelsTemplates.xc', XVMChecked('itemXvmBattleMMHT10'), '${"SuperHeavyTank.Spotted.False', '${"SuperHeavyTank.Spotted.True');
 ConfigEdit(True, XVM, 'Minimap\minimapLabelsTemplates.xc', XVMChecked('itemXvmBattleMMHT10'), '${"SuperHeavyTank.Lost.False', '${"SuperHeavyTank.Lost.True');
 ConfigEdit(True, XVM, 'Minimap\minimap.xc', XVMChecked('itemXvmBattleMMCenter'), '"centered": false', '"centered": true');
 ConfigEdit(True, XVM, 'hotkeys.xc', XVMChecked('itemXvmBattleMMCenter'), '"minimapZoom": { "enabled": false', '"minimapZoom": { "enabled": true');
 ConfigEdit(True, XVM, 'hotkeys.xc', XVMChecked('itemXvmBattleMMCenterCtrl'), '"keyCode": 58', '"keyCode": 29');
 ConfigEdit(True, XVM, 'hotkeys.xc', XVMChecked('itemXvmBattleMMCenterAlt'), '"keyCode": 58', '"keyCode": 56');

 ConfigEdit(True, XVM, 'Minimap\minimapLabelsAlt.xc', XVMChecked('itemXvmBattleMMAlt'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'hotkeys.xc', XVMChecked('itemXvmBattleMMAlt'), '"minimapAltMode": { "enabled": false', '"minimapAltMode": { "enabled": true');
 ConfigEdit(True, XVM, 'Minimap\minimapLabelsTemplates.xc', XVMChecked('itemXvmBattleMMAltNicks'), '"enabled": false, //Nick', '"enabled": true, //Nick');

 ConfigEdit(True, XVM, 'playersPanel.xc', XVMChecked('itemXvmBattlePPHP'), '"enabled": false, //HPBar', '"enabled": true, //HPBar');

 ConfigEdit(True, XVM, 'playersPanel.xc', XVMChecked('itemXvmBattlePPSpotted'), '"enabled": false, //SpottedMarker', '"enabled": true, //SpottedMarker');
 ConfigEdit(True, XVM, 'texts.xc', XVMChecked('itemXvmBattlePPSpottedDot'), 'SpottedText.Default', 'SpottedText.Dots');
 ConfigEdit(True, XVM, 'texts.xc', XVMChecked('itemXvmBattlePPSpottedStar'), 'SpottedText.Default', 'SpottedText.Stars');
 ConfigEdit(True, XVM, 'texts.xc', XVMChecked('itemXvmBattlePPDead'), 'SpottedText.DeadSkulls.False', 'SpottedText.DeadSkulls.True');

 ConfigEdit(True, XVM, 'Markers\MarkersTemplates.xc', XVMChecked('itemXvmBattleVMLowHP'), '"enabled": false, //SOS', '"enabled": true, //SOS');
 ConfigEdit(True, XVM, 'Markers\MarkersTemplates.xc', XVMChecked('itemXvmBattleVMMastery'), '"enabled": false, //marksOfMastery', '"enabled": true, //marksOfMastery');
 ConfigEdit(True, XVM, 'Markers\MarkersTemplates.xc', XVMChecked('itemXvmBattleVMAllySpotted'), '"enabled": false, //XMQP', '"enabled": true, //XMQP');
 ConfigEdit(True, XVM, 'Markers\MarkersTemplates.xc', XVMChecked('itemXvmBattleVMServices'), '"enabled": false, //OnlineServices', '"enabled": true, //OnlineServices');
 ConfigEdit(True, XVM, 'Markers\MarkersTemplates.xc', XVMChecked('itemXvmBattleVMSquadInStd'), '"enabled": false, //SquadIconNormal', '"enabled": true, //SquadIconNormal');
 ConfigEdit(True, XVM, 'Markers\MarkersTemplates.xc', XVMChecked('itemXvmBattleVMSquadInAlt'), '"enabled": false, //SquadIconExtended', '"enabled": true, //SquadIconExtended');

 ConfigEdit(True, XVM, 'sight.xc', XVMChecked('itemXvmBattlePySphere'), '"enabled": false, //sphere', '"enabled": true, //sphere');
 ConfigEdit(True, XVM, 'battleMessages.xc', XVMChecked('itemXvmBattlePyTeamDamage') or XVMChecked('itemXvmBattlePyIAmSpotted'), '"enabled": false, //base', '"enabled": true, //base');
 ConfigEdit(True, XVM, 'BattleLabels\BL_Blocked.xc', XVMChecked('itemXvmBattlePyBlocked'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'BattleLabels\BL_Assisted.xc', XVMChecked('itemXvmBattlePyAssist'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'BattleLabels\BL_RepairTime.xc', XVMChecked('itemXvmBattlePyRepairTimer'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'BattleLabels\BL_RepairControl.xc', XVMChecked('itemXvmBattlePyRepairControl'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'battleMessages.xc', XVMChecked('itemXvmBattlePyTeamDamage'), '"enabled": false, //td', '"enabled": true, //td');
 ConfigEdit(True, XVM, 'safeShot.xc', XVMChecked('itemXvmBattlePySafeShot'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'hitLog.xc', XVMChecked('itemXvmBattlePyHitlog'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'BattleLabels\BL_HitLog.xc', XVMChecked('itemXvmBattlePyHitlog'), '"enabled": false, //header', '"enabled": true, //header');
 ConfigEdit(True, XVM, 'BattleLabels\BL_HitLog.xc', XVMChecked('itemXvmBattlePyHitlogDetailed'), '"enabled": false, //body', '"enabled": true, //body');
 ConfigEdit(True, XVM, 'BattleLabels\BL_DamageLog.xc', XVMChecked('itemXvmBattlePyDamagelog'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'damageLog.xc', XVMChecked('itemXvmBattlePyDamagelog'), '"moveInBattle": false', '"moveInBattle": true');
 ConfigEdit(True, XVM, 'damageLog.xc', not XVMChecked('itemXvmBattlePyDamagelog'), '"disabledDetailStats": true', '"disabledDetailStats": false');
 ConfigEdit(True, XVM, 'damageLog.xc', not XVMChecked('itemXvmBattlePyDamagelog'), '"disabledSummaryStats": true', '"disabledSummaryStats": false');
 ConfigEdit(True, XVM, 'BattleLabels\BL_InfoPanel.xc', XVMChecked('itemXvmBattlePyInfoPanel'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'infoPanel.xc', XVMChecked('itemXvmBattlePyInfoPanel'), '"enabled": false, //ip', '"enabled": true, //ip');
 ConfigsEdit(True, XVM, ['infoPanel.xc', 'battleLabels.xc'], XVMChecked('itemXvmBattlePyInfoPanelNDO'), 'def.full.', 'def.ndo.');
 ConfigsEdit(True, XVM, ['infoPanel.xc', 'battleLabels.xc'], XVMChecked('itemXvmBattlePyInfoPanelKMP'), 'def.full.', 'def.kmp.');
 ConfigsEdit(True, XVM, ['infoPanel.xc', 'battleLabels.xc'], XVMChecked('itemXvmBattlePyInfoPanelLite'), 'def.full.', 'def.lite.');
 ConfigsEdit(True, XVM, ['battleEfficiency.xc', 'BattleLabels\BL_BattleEfficiency.xc'], XVMChecked('itemXvmBattlePyCalc'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('itemXvmBattlePyCalcWN8'), 'Templates.Empty', 'Templates.WN8');
 ConfigEdit(True, XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('itemXvmBattlePyCalcEFF'), 'Templates.Empty', 'Templates.EFF');
 ConfigEdit(True, XVM, 'BattleLabels\BL_BattleEfficiency.xc', XVMChecked('itemXvmBattlePyCalcEFFWN8'), 'Templates.Empty', 'Templates.WN8EFF');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGN'), '"enabled": false', '"enabled": true');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGNOctagon'), '1_Left', '2_Left');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGNOctagon'), '1_Right', '2_Right');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGNPrnts'), '1_Left', '3_Left');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGNPrnts'), '1_Right', '3_Right');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGNPrnts2'), '1_Left', '4_Left');
 ConfigEdit(True, XVM, 'BattleLabels\BL_AngleAiming.xc', XVMChecked('itemXvmBattlePyUGNPrnts2'), '1_Right', '4_Right');
 ConfigEdit(True, XVM, 'battleMessages.xc', XVMChecked('itemXvmBattlePyIAmSpotted'), '"enabled": false, //ias', '"enabled": true, //ias');
 ConfigEdit(True, XVM, 'battleMessages.xc', XVMChecked('itemXvmBattlePyIAmSpotted15'), '"showWhenLess": 6', '"showWhenLess": 0');

 ConfigEdit(True, XVM, 'battle.xc', XVMChecked('itemXvm6Sense'), '"sixthSenseIcon": ""', '"sixthSenseIcon": "xvm://res/SixthSense.png"');
 ConfigEdit(True, XVM, 'battle.xc', XVMChecked('itemXvm6Sens9Sec'), '"sixthSenseDuration": 2000', '"sixthSenseDuration": 9000');
//
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6Sense') or XVMChecked('itemXvmSoundCrit') or XVMChecked('itemXvmSoundExt'), '"enabled": false', '"enabled": true');

 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6SenseStd'), '"xvm_sixthSense": "xvm_sixthSense"', '"xvm_sixthSense": "SixthSense_Standard"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6SenseVoice'), '"xvm_sixthSense": "xvm_sixthSense"', '"xvm_sixthSense": "SixthSense_Voice"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6SenseRudy'), '"xvm_sixthSense": "xvm_sixthSense"', '"xvm_sixthSense": "SixthSense_Rudy"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6SenseWCountdown'), '"xvm_sixthSense": "xvm_sixthSense"', '"xvm_sixthSense": "SixthSense_SignalTimer"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6SenseWOCountdown'), '"xvm_sixthSense": "xvm_sixthSense"', '"xvm_sixthSense": "SixthSense_Signal"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSound6SenseEye'), '"xvm_sixthSense": "xvm_sixthSense"', '"xvm_sixthSense": "SixthSense_Sauron"');

 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundCritRing'), '/** //Crit_Damaged', '//Crit_Damaged');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundCritRing'), '*/ //Crit_Damaged', '//Crit_Damaged');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundCritRingVoice'), '/** //Crit_Damaged_Voice', '//Crit_Damaged_Voice');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundCritRingVoice'), '*/ //Crit_Damaged_Voice', '//Crit_Damaged_Voice');

 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundExtEnemySpotted'), '//"enemy_sighted_for_team"', '"enemy_sighted_for_team"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundExtEnemySpotted'), '"xvm_enemySighted": ""', '"xvm_enemySighted": "Other_EnemySighted"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundExtFire'), '//"vo_fire_started"', '"vo_fire_started"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundExtFire'), '"xvm_fireAlert": ""', '"xvm_fireAlert": "Other_FireAlert"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundExtAmmoBay'), '//"vo_ammo_bay_damaged"', '"vo_ammo_bay_damaged"');
 ConfigEdit(True, XVM, 'sounds.xc', XVMChecked('itemXvmSoundExtAmmoBay'), '"xvm_ammoBay": ""', '"xvm_ammoBay": "Other_AmmoBay"');
end;

Procedure RememberXVMItems(CurStep: TSetupStep);
begin
 if (CurStep = ssPostInstall) and CheckBoxGetChecked(CBParamsRemember) then
  InstallParams_Mods(CMD_SET, XVMList);
end;
