﻿// © Kotyarko_O, 2020 \\

#include "..\l10n\cm_itemsBase.iss"

 #ifndef TESTING
[Files]
Source: "mods\frameworks\configs\*"; DestDir: "{app}\mods\configs"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase');
Source: "mods\frameworks\*.wotmod"; DestDir: "{app}\mods\{#Patch}"; Excludes: "configs"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase');
Source: "mods\frameworks\*.pyc"; DestDir: "{app}\res_mods\{#Patch}\scripts\client\gui\mods"; Excludes: "configs"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase');

Source: "mods\updater\kmp.updater.wotmod"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusBase');
Source: "mods\updater\kmp.updater.wotmod"; DestDir: "{app}\mods\{code:GetPatchVersionIncreased}"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusBase');
Source: "mods\updater\updaterClient.json"; DestDir: "{app}\mods\configs\updater"; Flags: ignoreversion; BeforeInstall: SetInstallStatus('instStatusBase');

Source: "mods\contours\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseContours');

Source: "mods\hangar\wargaming_fm\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseHangarFM');
//Source: "mods\hangar\hangar_manager\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseHangarManager');
Source: "mods\hangar\replays_manager\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseHangarReplays');
Source: "mods\hangar\crew_skills\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseHangarSkills');
Source: "mods\hangar\vert_tree\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseHangarVertTree');
Source: "mods\hangar\gold_prem_tanks\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseHangarPremIcons');

Source: "mods\crash_textures\mod.MCTCreator.TankCrashTextures_*.wotmod"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseMCTTanks');
Source: "mods\crash_textures\mod.MCTCreator.TrackCrashTextures*.wotmod"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseMCTTracks');

Source: "mods\OTHER\battle_assistant\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtBA');
Source: "mods\OTHER\frag_corellation_panel_mini\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtFCPanel');
Source: "mods\OTHER\server_turret\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtSTurret');
Source: "mods\OTHER\paintball\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtPaintball');
Source: "mods\OTHER\killog_icons\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtKillog');
Source: "mods\OTHER\antitoxicity\*"; DestDir: "{app}\res_mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtAntitoxicity');
Source: "mods\OTHER\camo_selector\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusBase'); Check: ComponentsChecked('itemBaseExtCamo');
#endif

[Code]
Var
 ComponentsPage: TWizardPage;
 ComponentsList: TNewCheckListBox;
 ComponentsPageImg, ComponentsPageNameImg: Longint;

Function ComponentsChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(ComponentsList, Name);
end;

Procedure SetComponentsPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(ComponentsPageImg, Value);
 ImgSetVisibility(ComponentsPageNameImg, Value);
 ResetPreviewImage(Value);
 ComponentsList.Visible := Value;
 DescriptionMemo.Visible := Value;
end;

Procedure ComponentsPageOnActivate(Sender: TWizardPage);
begin
 SetComponentsPageVisibility(True);
end;

Function ComponentsPageOnButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetComponentsPageVisibility(False);
end;

Procedure InitializeComponentsPage();
begin
 ComponentsPage := CreateCustomPage(SelectDirPage.ID, '', '');
 with ComponentsPage do
 begin
  OnActivate := @ComponentsPageOnActivate;
  OnBackButtonClick := @ComponentsPageOnButtonClick;
  OnNextButtonClick := @ComponentsPageOnButtonClick;
 end;

 ComponentsPageImg := ImgLoad(WizardForm.Handle, 'pageComponents.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 ComponentsPageNameImg := ImgLoad(WizardForm.Handle, Format('pageComponentsName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 ComponentsList := TNewCheckListBox.Create(ComponentsPage);
 with ComponentsList do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(10), ScaleY(93), ScaleX(455), ScaleY(389));
  Offset := 2;
  Name := 'ComponentsList';
  Tag := 0;
  Font.Size := 9;
 end;

 SetCheckListBoxItemsInfo(ComponentsList.Tag);
 SetCheckListBoxBGBMP(ComponentsList);
 SetCheckListBoxEvents(ComponentsList);
 SetComponentsPageVisibility(False);

 AddCheckBoxExt(ComponentsList, 'itemBaseContours', 0, True, [fsBold], 'Contours.png');
  AddRadioButtonExt(ComponentsList, 'itemBaseContoursWhite', 1, True, [], 'White.jpg');
  AddRadioButtonExt(ComponentsList, 'itemBaseContoursYellow', 1, True, [], 'Yellow.jpg');
  AddRadioButtonExt(ComponentsList, 'itemBaseContoursBlue', 1, True, [], 'Blue.jpg');

 AddCheckBoxExt(ComponentsList, 'itemBaseHangar', 0, True, [fsBold], 'Hangar.png');
  AddCheckBoxExt(ComponentsList, 'itemBaseHangarFM', 1, True, [], 'Radio.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseHangarManager', 1, False, [], 'HangarManager.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseHangarReplays', 1, True, [], 'ReplaysManager.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseHangarSkills', 1, ActiveLanguage() = 'ru', [], 'SkillsDescs.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseHangarVertTree', 1, False, [], 'VerticalTree.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseHangarPremIcons', 1, True, [], 'GoldPrems.jpg');

 AddCheckBoxExt(ComponentsList, 'itemBaseMCT', 0, True, [fsBold], 'MCTCreator.png');
  AddCheckBoxExt(ComponentsList, 'itemBaseMCTTanks', 1, True, [], 'TanksAndTracks.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseMCTTracks', 1, True, [], 'TanksAndTracks.jpg');

 AddCheckBoxExt(ComponentsList, 'itemBaseExt', 0, True, [fsBold], 'Other.png');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtBA', 1, True, [], 'BattleAssistant.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtFCPanel', 1, True, [], 'ScorePanel.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtSTurret', 1, True, [], 'ServerTurret.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtPaintball', 1, True, [], 'Paintball.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtKillog', 1, ActiveLanguage() = 'ru', [], 'Killog.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtAntitoxicity', 1, True, [], 'Antitoxicity.jpg');
  AddCheckBoxExt(ComponentsList, 'itemBaseExtCamo', 1, False, [], 'RandomCamo.jpg');

 InstallParams_Mods(CMD_GET, ComponentsList);
end;

Procedure ConfigurateComponents();
begin
 ConfigEdit(True, UPDATER, 'updaterClient.json', True, '"0.0"', '"{#Version}"');
 ConfigEdit(True, UPDATER, 'updaterClient.json', CheckBoxGetChecked(CBClientGA), '"enableAnalytics": false', '"enableAnalytics": true');

 ConfigEdit(False, EDGE, 'EdgeDetectLite.xml', ComponentsChecked('itemBaseContoursWhite'), '<color>255 18 7 255</color>', '<color>255 255 255 255</color>');
 ConfigEdit(False, EDGE, 'EdgeDetectLite.xml', ComponentsChecked('itemBaseContoursYellow'), '<color>255 18 7 255</color>', '<color>207 146 49 255</color>');
 ConfigEdit(False, EDGE, 'EdgeDetectLite.xml', ComponentsChecked('itemBaseContoursBlue'), '<color>255 18 7 255</color>', '<color>130 120 253 255</color>');
end;

Procedure RememberComponentItems(CurStep: TSetupStep);
begin
 if (CurStep = ssPostInstall) and CheckBoxGetChecked(CBParamsRemember) then
  InstallParams_Mods(CMD_SET, ComponentsList);
end;
