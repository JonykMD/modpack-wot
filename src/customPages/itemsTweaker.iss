﻿// © Kotyarko_O, 2020 \\

#include "..\l10n\cm_itemsTweaker.iss"

 #ifndef TESTING
[Files]
//Source: "mods\tweaks\_hotfix\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker');

Source: "mods\tweaks\emblems_off\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerEmblems');
Source: "mods\tweaks\fog\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerFog');
Source: "mods\tweaks\tank_destroy_smoke\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerDestroyedSmoke');
Source: "mods\tweaks\tank_shoot_smoke\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerShootSmoke');
Source: "mods\tweaks\tank_smoke\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerExhaustSmoke');
Source: "mods\tweaks\skyboxes\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerSkybox');
Source: "mods\tweaks\trees\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerTrees');
Source: "mods\tweaks\shells\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerShells');
Source: "mods\tweaks\tank_hit\*"; DestDir: "{app}\mods\{#Patch}"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusTweaker'); Check: TweakerChecked('itemTweakerTankHit');
#endif

[Code]
Var
 TweakerPage: TWizardPage;
 TweakerList: TNewCheckListBox;
 TweakerPageImg, TweakerPageNameImg: Longint;

Function TweakerChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(TweakerList, Name);
end;

Procedure SetTweakerPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(TweakerPageImg, Value);
 ImgSetVisibility(TweakerPageNameImg, Value);
 ResetPreviewImage(Value);
 TweakerList.Visible := Value;
 DescriptionMemo.Visible := Value;
end;

Procedure TweakerPageOnActivate(Sender: TWizardPage);
begin
 SetTweakerPageVisibility(True);
end;

Function TweakerPageOnButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetTweakerPageVisibility(False);
end;

Procedure InitializeTweakerPage();
begin
 TweakerPage := CreateCustomPage(PMODPage.ID, '', '');
 with TweakerPage do
 begin
  OnActivate := @TweakerPageOnActivate;
  OnBackButtonClick := @TweakerPageOnButtonClick;
  OnNextButtonClick := @TweakerPageOnButtonClick;
 end;

 TweakerPageImg := ImgLoad(WizardForm.Handle, 'pageTweaker.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 TweakerPageNameImg := ImgLoad(WizardForm.Handle, Format('pageTweakerName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 TweakerList := TNewCheckListBox.Create(TweakerPage);
 with TweakerList do
 begin
  Parent := WizardForm;
  SetBounds(ComponentsList.Left, ComponentsList.Top, ComponentsList.Width, ComponentsList.Height);
  Offset := 2;
  Name := 'TweakerList';
  Tag := 3;
  Font.Size := ComponentsList.Font.Size;
 end;

 SetCheckListBoxItemsInfo(TweakerList.Tag);
 SetCheckListBoxBGBMP(TweakerList);
 SetCheckListBoxEvents(TweakerList);
 SetTweakerPageVisibility(False);

 AddCheckBoxExt(TweakerList, 'itemTweaker', 0, True, [fsBold], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerEmblems', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerFog', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerDestroyedSmoke', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerShootSmoke', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerExhaustSmoke', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerSkybox', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerTrees', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerShells', 1, True, [], 'Tweaker.png');
 AddCheckBoxExt(TweakerList, 'itemTweakerTankHit', 1, True, [], 'Tweaker.png');

 InstallParams_Mods(CMD_GET, TweakerList);
end;

Procedure RememberTweakerItems(CurStep: TSetupStep);
begin
 if (CurStep = ssPostInstall) and CheckBoxGetChecked(CBParamsRemember) then
  InstallParams_Mods(CMD_SET, TweakerList);
end;
