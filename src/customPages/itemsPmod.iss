﻿// © Kotyarko_O, 2020 \\

#include "..\l10n\cm_itemsPmod.iss"

 #ifndef TESTING
[Files]
Source: "mods\pmod\*"; DestDir: "{app}\mods"; Flags: ignoreversion recursesubdirs createallsubdirs; BeforeInstall: SetInstallStatus('instStatusPmod'); Check: PMODChecked('itemPmod');
#endif

[Code]
Var
 PMODPage: TWizardPage;
 PMODList: TNewCheckListBox;
 PMODPageImg, PMODPageNameImg: Longint;

Function PMODChecked(Name: String): Boolean;
begin
 Result := _IsComponentSelected(PMODList, Name);
end;

Procedure SetPMODPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(PMODPageImg, Value);
 ImgSetVisibility(PMODPageNameImg, Value);
 ResetPreviewImage(Value);
 PMODList.Visible := Value;
 DescriptionMemo.Visible := Value;
end;

Procedure PMODPageOnActivate(Sender: TWizardPage);
begin
 SetPMODPageVisibility(True);
end;

Function PMODPageOnButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetPMODPageVisibility(False);
end;

Procedure InitializePMODPage();
begin
 PMODPage := CreateCustomPage(XVMPage.ID, '', '');
 with PMODPage do
 begin
  OnActivate := @PMODPageOnActivate;
  OnBackButtonClick := @PMODPageOnButtonClick;
  OnNextButtonClick := @PMODPageOnButtonClick;
 end;

 PMODPageImg := ImgLoad(WizardForm.Handle, 'pagePmod.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 PMODPageNameImg := ImgLoad(WizardForm.Handle, Format('pagePmodName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 PMODList := TNewCheckListBox.Create(PMODPage);
 with PMODList do
 begin
  Parent := WizardForm;
  SetBounds(ComponentsList.Left, ComponentsList.Top, ComponentsList.Width, ComponentsList.Height);
  Offset := 2;
  Name := 'PMODList';
  Tag := 2;
  Font.Size := ComponentsList.Font.Size;
 end;

 SetCheckListBoxItemsInfo(PMODList.Tag);
 SetCheckListBoxBGBMP(PMODList);
 SetCheckListBoxEvents(PMODList);
 SetPMODPageVisibility(False);

 AddCheckBoxExt(PMODList, 'itemPmod', 0, True, [fsBold], 'PMOD.png');
 AddCheckBoxExt(PMODList, 'itemPmodBinoculars', 1, True, [], 'SniperShadow.jpg');
 AddCheckBoxExt(PMODList, 'itemPmodDynamic', 1, True, [], 'DynamicCamera.png');
 AddCheckBoxExt(PMODList, 'itemPmodStabilizer', 1, True, [], 'HorizontalStabilization.png');
 AddCheckBoxExt(PMODList, 'itemPmodFreeCam', 1, True, [], 'FreeCam.jpg');
 AddCheckBoxExt(PMODList, 'itemPmodCommandersCam', 1, True, [], 'ComCam.jpg');
 AddCheckBoxExt(PMODList, 'itemPmodHitFlash', 1, True, [], 'RedBlink.jpg');
 AddCheckBoxExt(PMODList, 'itemPmodBrakeRemove', 1, True, [], 'HandBrake.png');
 AddCheckBoxExt(PMODList, 'itemPmodPenetration', 1, True, [], 'ArmorIndicator.jpg');
 AddCheckBoxExt(PMODList, 'itemPmodFilter', 1, True, [], 'MessagesFilter.png');

 AddCheckBoxExt(PMODList, 'itemPmodServerSight', 1, True, [fsBold], 'ServerCrosshair.png');
 AddRadioButtonExt(PMODList, 'itemPmodServerSightStd', 2, True, [], 'ServerCrosshair_Std.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodServerSightModern', 2, True, [], 'ServerCrosshair_Alt.jpg');

 AddCheckBoxExt(PMODList, 'itemPmodZoom', 1, True, [fsBold], 'SniperZoom.png');
 AddRadioButtonExt(PMODList, 'itemPmodZoomX32', 2, True, [], 'ZoomX32.png');
 AddRadioButtonExt(PMODList, 'itemPmodZoomX50', 2, True, [], 'ZoomX50.png');

 AddCheckBoxExt(PMODList, 'itemPmodDefZoom', 1, True, [fsBold], 'SniperDefZoom.png');
 AddRadioButtonExt(PMODList, 'itemPmodDefZoomX2', 2, True, [], 'SniperDefZoom_2х.png');
 AddRadioButtonExt(PMODList, 'itemPmodDefZoomX6', 2, True, [], 'SniperDefZoom_6х.png');
 AddRadioButtonExt(PMODList, 'itemPmodDefZoomX24', 2, True, [], 'SniperDefZoom_24х.png');

 AddCheckBoxExt(PMODList, 'itemPmodTeamHP', 1, True, [fsBold], 'TeamHP.png');
 AddRadioButtonExt(PMODList, 'itemPmodTeamHPStd', 2, True, [], 'TeamHP_Default.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodTeamHPLocastan', 2, True, [], 'TeamHP_Locastan.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodTeamHPMin', 2, True, [], 'TeamHP_Minimal.jpg');

 AddCheckBoxExt(PMODList, 'itemPmodSession', 1, True, [fsBold], 'SessionStatistic.png');
 AddRadioButtonExt(PMODList, 'itemPmodSessionStd', 2, True, [], 'Standard.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionExt', 2, True, [], 'StandardExt.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionKMP', 2, True, [], 'Kotyarko_O.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionCSSR', 2, True, [], 'vlad_cs_sr.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionMeddio', 2, True, [], 'Meddio.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionMutant', 2, True, [], 'Xxx_mutant.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionDemon', 2, True, [], 'demon2597.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionAntoshka', 2, True, [], 'Antoshkaaa.jpg');
 AddRadioButtonExt(PMODList, 'itemPmodSessionArmagomen', 2, True, [], 'Armagomen.jpg');

 InstallParams_Mods(CMD_GET, PMODList);
end;

Procedure ConfiguratePMOD();
begin
 if not PMODChecked('CheckForChecked') then
  Exit;
//
 ConfigEdit(False, PMOD, 'noBinoculars.json', PMODChecked('itemPmodBinoculars'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'noDynamic.json', PMODChecked('itemPmodDynamic'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'horizontalStabilizer.json', PMODChecked('itemPmodStabilizer'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'videomode.json', PMODChecked('itemPmodFreeCam'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'zoomDistance.json', PMODChecked('itemPmodCommandersCam'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'cameraOptions.json', PMODChecked('itemPmodHitFlash'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'cameraOptions.json', PMODChecked('itemPmodHitFlash'), '"noFlashBang": false', '"noFlashBang": true');
 ConfigEdit(False, PMOD, 'TDBrakeRemover.json', PMODChecked('itemPmodBrakeRemove'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'TDBrakeRemover.json', PMODChecked('itemPmodBrakeRemove'), '"TDSniperMovementMode": "custom"', '"TDSniperMovementMode": "full"');
 ConfigEdit(False, PMOD, 'reducedArmor.json', PMODChecked('itemPmodPenetration'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'messagesCleaner.json', PMODChecked('itemPmodFilter'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'messagesCleaner.json', PMODChecked('itemPmodFilter'), '"popUpMessages": false', '"popUpMessages": true');
 ConfigEdit(False, PMOD, 'messagesCleaner.json', PMODChecked('itemPmodFilter'), '"systemLog": false', '"systemLog": true');

 ConfigEdit(False, PMOD, 'serverDispersionCircle.json', PMODChecked('itemPmodServerSight'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'serverDispersionCircle.json', PMODChecked('itemPmodServerSightModern'), 'standart.dds', 'blue.dds');

 ConfigEdit(False, PMOD, 'zoomX.json', PMODChecked('itemPmodZoom'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'zoomX.json', PMODChecked('itemPmodZoomX32'), '"zoomXSteps": [2, 4, 8, 16]', '"zoomXSteps": [2, 4, 6, 8, 10, 16, 20, 24, 32]');
 ConfigEdit(False, PMOD, 'zoomX.json', PMODChecked('itemPmodZoomX50'), '"zoomXSteps": [2, 4, 8, 16]', '"zoomXSteps": [2, 4, 8, 12, 20, 28, 34, 40, 50]');

 ConfigEdit(False, PMOD, 'defaultZoom.json', PMODChecked('itemPmodDefZoom'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'defaultZoom.json', PMODChecked('itemPmodDefZoomX2'), '"defaultZoomValue": 4', '"defaultZoomValue": 2');
 ConfigEdit(False, PMOD, 'defaultZoom.json', PMODChecked('itemPmodDefZoomX6'), '"defaultZoomValue": 4', '"defaultZoomValue": 6');
 ConfigEdit(False, PMOD, 'defaultZoom.json', PMODChecked('itemPmodDefZoomX24'), '"defaultZoomValue": 4', '"defaultZoomValue": 24');

 ConfigEdit(False, PMOD, 'battleGui.json', PMODChecked('itemPmodTeamHP'), '"enable": false, //HP', '"enable": true, //HP');
 ConfigEdit(False, PMOD, 'battleGui.json', PMODChecked('itemPmodTeamHPStd'), '"displayDamage": true', '"displayDamage": false');
 ConfigEdit(False, PMOD, 'battleGui.json', PMODChecked('itemPmodTeamHPLocastan'), '"skin": "default"', '"skin": "locastan"');
 ConfigEdit(False, PMOD, 'battleGui.json', PMODChecked('itemPmodTeamHPMin'), '"skin": "default"', '"skin": "minimal"');

 ConfigsEdit(False, PMOD, ['battleChat.json', 'sessionStatistic.json', 'systemMessages.json'], PMODChecked('itemPmodSession'), '"enable": false', '"enable": true');
 ConfigEdit(False, PMOD, 'sessionStatistic.json', PMODChecked('itemPmodSession'), '"autoReset": false', '"autoReset": true');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSession') and (AnsiLowercase(ActiveLanguage()) = 'en'), '_ru.json', '_en.json');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionExt'), 'default/sessionStatistic_', 'default\sessionStatisticExt_');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionKMP'), 'default', 'Kotyarko_O');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionCSSR'), 'default', 'vlad_cs_sr');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionMeddio'), 'default', 'Meddio');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionMutant'), 'default', 'XXX_MUTANT');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionDemon'), 'default', 'demon2597');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionAntoshka'), 'default', 'Antoshkaaa');
 ConfigEdit(False, PMOD, 'skins\skinLoader.json', PMODChecked('itemPmodSessionArmagomen'), 'default', 'Armagomen');
end;

Procedure RememberPMODItems(CurStep: TSetupStep);
begin
 if (CurStep = ssPostInstall) and CheckBoxGetChecked(CBParamsRemember) then
  InstallParams_Mods(CMD_SET, PMODList);
end;
