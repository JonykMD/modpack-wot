﻿// © Kotyarko_O, 2020 \\

[Code]
Function PreparingShouldSkipPage(): Boolean;
var
 ResultCode: Integer;
begin
 Exec(ExpandConstant('{cmd}'), 'taskkill /IM WorldOfTanks.exe', '', SW_SHOW, ewWaitUntilTerminated, ResultCode);

 Result := True;
end;
