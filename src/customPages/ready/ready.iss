﻿// © Kotyarko_O, 2020 \\

#include "readyMemo.iss"

[Code]
Var
 ReadyPage: TWizardPage;
 ReadyPageImg, ReadyPageNameImg: Longint;
 ReadyLabel: TLabel;
 ReadyMemo: TNewMemo;

Procedure SaveReadyMemoToLog(const CurStep: TSetupStep);
var
 FileName: String;
begin
 if CurStep = ssPostInstall then
 begin
  FileName := ExpandConstant('{app}\kmp\install_log.kmp');
  ReadyMemo.Lines.SaveToFile(FileName);
  SaveStringToFile(FileName, #13#10#13#10 +
   '-- {#AppFullName} for {#GameFullName} {#Patch}' + #13#10 +
   '-- v-{#Version} - {#CreateDate}' + #13#10 +
   '-- Install Log of - ' + GetDateTimeString('dd/mm/yyyy hh:nn:ss', '.', ':'), True);
 end;
end;

Procedure SetReadyPageVisibility(Value: Boolean);
begin
 ImgSetVisibility(ReadyPageImg, Value);
 ImgSetVisibility(ReadyPageNameImg, Value);
 ReadyLabel.Visible := Value;
 ReadyMemo.Visible := Value;
end;

Procedure ReadyPageOnActivate(Sender: TWizardPage);
begin
 SetReadyPageVisibility(True);
 ReadyMemo.Text := GetReadyMemoFormat();
 WizardForm.NextButton.Caption := SetupMessage(msgButtonInstall);
 WizardForm.NextButton.Enabled := ComponentsChecked('CheckForChecked') or XVMChecked('CheckForChecked') or PMODChecked('CheckForChecked') or TweakerChecked('CheckForChecked');
end;

Function ReadyPageOnButtonClick(Sender: TWizardPage): Boolean;
begin
 Result := True;
 SetReadyPageVisibility(False);
 WizardForm.NextButton.Caption := SetupMessage(msgButtonNext);
end;

Procedure InitializeReadyPage();
begin
 ReadyPage := CreateCustomPage(TweakerPage.ID, '', '');
 with ReadyPage do
 begin
  OnActivate := @ReadyPageOnActivate;
  OnBackButtonClick := @ReadyPageOnButtonClick;
  OnNextButtonClick := @ReadyPageOnButtonClick;
 end;

 ReadyLabel := TLabel.Create(ReadyPage);
 with ReadyLabel do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(15), ScaleY(94), 0, 0);
  AutoSize := True;
  Transparent := True;
  WordWrap := False;
  Font.Size := 11;
  Font.Style := [fsBold];
  Caption := CustomMessage('readyLabelText');
 end;

 ReadyMemo := TNewMemo.Create(ReadyPage);
 with ReadyMemo do
 begin
  Parent := WizardForm;
  SetBounds(ScaleX(10), ScaleY(130), WizardForm.ClientWidth - ScaleX(20), ScaleY(360));
  ScrollBars := ssVertical;
  ReadOnly := True;
  Font.Size := 9;
 end;

 ReadyPageImg := ImgLoad(WizardForm.Handle, 'pageReady.png', 0, 0, WizardForm.ClientWidth, WizardForm.ClientHeight, True, True);
 ReadyPageNameImg := ImgLoad(WizardForm.Handle, Format('pageReadyName_%s.png', [ActiveLanguage()]), ScaleX(5), ScaleY(7), ScaleX(460), ScaleY(70), True, False);

 SetReadyPageVisibility(False);
end;