:: © Kotyarko_O, 2019 ::

@Echo off
Color 1F

CD ..
Set Own=%CD%
CD ..
Set UpperOwn=%CD%

Set DateTemp=%date:~-10%
Set BasicBackupFolder=%Own%\KMP-BasicBackup(%DateTemp%)
Set GeneralBackupFolder=%UpperOwn%\KMP-GeneralBackup(%DateTemp%)

:Menu
	Set Answer=
	CD %Own%
	Cls
 	Echo+
 	Echo ------------------------------------------------------------------------------------
	Echo   Working Dir: "%Own%\backuper.bat"
	Echo+
	Echo   Backup-Basic Dir: "%BasicBackupFolder%"
	Echo   Backup-General Dir: "%GeneralBackupFolder%"
	Echo ------------------------------------------------------------------------------------
	Echo+
	Echo ^>Hello %USERNAME%, what do you want to do?
	Echo+
	Echo ^>Choose:
	Echo    A) Backup-Basic;
	Echo    B) Backup-General;
	Echo    Q) Quit (Exit).

:Choise
	Echo+
	Set /P Answer= ^>Select option: 
	Set Answer=%Answer:~0,1%
	If /I "%Answer%"=="Q" Exit
	If /I "%Answer%"=="A" GOTO:Backup-Basic
	If /I "%Answer%"=="B" GOTO:Backup-General
	GOTO:Menu 

:Backup-Basic
	If EXIST "%BasicBackupFolder%" (
		RD /S /Q "%BasicBackupFolder%"
	)
	MD "%BasicBackupFolder%"
	
	XCopy ".\files"	"%BasicBackupFolder%\files" /E /I /Y
	XCopy ".\src"	"%BasicBackupFolder%\src" /E /I /Y
	XCopy ".\utils"	"%BasicBackupFolder%\utils" /E /I /Y
	For %%a in ("%Own%\*.iss") do copy "%%a" "%BasicBackupFolder%\" /V
	GOTO:END

:Backup-General
	If EXIST "%GeneralBackupFolder%" (
		RD /S /Q "%GeneralBackupFolder%"
	)
	MD "%GeneralBackupFolder%"
	
	XCopy ".\*" "%GeneralBackupFolder%" /E /I /Y
	GOTO:END

:END
	Echo+
	Echo+
	@Pause
	GOTO:Menu
