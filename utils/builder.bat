::It`s necessarry to have environment variable "InnoSetupEx551U", "ZIP",
::which contains full path to Compil32Ex.exe and 7z.exe applications

@Echo off

Set OutputName=Kotyarko_O`s ModPack.zip

If Not DEFINED InnoSetupEx551U (
	@Echo "Error: The environment variable InnoSetupEx551U doesn`t exist."
	GOTO:_Exit
)
If Not DEFINED ZIP (
	@Echo "Error: The environment variable ZIP doesn`t exist."
	GOTO:_Exit
)

:_Prepare
	@Echo "Step: Prepairing..."
	
	CD ".\MCTCreator"
	CALL makeTextures.bat
	CD ".."
	
	CD "..\BUILDED"
	DEL /Q ".\*.exe"
	if EXIST ".\%OutputName%" (
		DEL /Q ".\%OutputName%"
	)

	@Echo+
	GOTO:_Build

:_Build
	@Echo "Step: Building..."
	
	"%InnoSetupEx551U%" /cc "..\main.iss"

	@Echo+
	GOTO:_Archivate

:_Archivate
	@Echo "Step: Archivating..."
	
	"%ZIP%" a -tzip "%OutputName%" ".\*.exe"
	
	@Echo+
	GOTO:_Exit

:_Exit
	@Echo+
	@Pause
	GOTO:EOF
