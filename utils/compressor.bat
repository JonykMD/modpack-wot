@Echo on
@Pause

Call iCatalyst\iCatalyst.bat /png:2 /jpg:2 "/outdir:false" "..\files\previews\*"

If EXIST "..\mods\xvm\tank_icons" (
	Call iCatalyst\iCatalyst.bat /png:2 "/outdir:false" "..\mods\xvm\tank_icons\*"
	Call iCatalyst\iCatalyst.bat /png:2 "/outdir:false" "..\mods\hangar\gold_prem_tanks\res\*"
)

@Echo+
@Pause
