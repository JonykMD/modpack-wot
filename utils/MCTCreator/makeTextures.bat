::It`s necessarry to have environment variables "WOT_ROOT",
::which contains full paths to WorldOfTanks root-folder

@Echo off

Set TexturesPath=..\..\mods\crash_textures\

If Not DEFINED WOT_ROOT (
	@Echo Error: The environment variable WOT_ROOT doesn`t exist.
	GOTO:_Exit
)

:_MakeTextures
	@Echo Please, wait. Generating corpses textures...
	MCTCreator2.exe /wot-path "%WOT_ROOT%" /mods-path "%TexturesPath%" /DPT-mod "#FFFFFF" /remove-wotmod /auto-close-console /no-update-configs
	@Echo+
	
	@Echo Please, wait. Generating tracks textures...
	MCTCreator2.exe /wot-path "%WOT_ROOT%" /mods-path "%TexturesPath%" /CCT-mod "#FFFFFF" /auto-close-console /no-update-configs
	@Echo+

:_Exit
	@Echo+
	GOTO:EOF
