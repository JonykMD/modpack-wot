﻿ // © Kotyarko_O, 2020 \\

#define Patch "1.9.0.3"
#define Version "3.27"

 #if FindFirst("mods\updater", faDirectory) == 0
#define TESTING
#endif

#define Compress
#define CheckForGameFiles
#define CheckForGameRun

#include "defines.iss"
#include "setup.iss"
#include "src\l10n\cm.iss"
#include "src\utils.iss"
#include "src\botva2\botva2.iss"
#include "src\xml.iss"
#include "src\idp\idp.iss"

#include "src\vcl\vcl.iss"
//#include "src\analytics\analytics.iss"

#include "src\window.iss"
#include "src\widgets.iss"
#include "src\paramsRememberer.iss"
#include "src\customPages\welcome.iss"
#include "src\customPages\updater\updater.iss"
#include "src\customPages\selectDir\selectDir.iss"
#include "src\configEditor.iss"
#include "src\previewSounds.iss"
#include "src\checkListBoxSrc.iss"
#include "src\customPages\itemsBase.iss"
#include "src\customPages\itemsXVM.iss"
#include "src\customPages\itemsPMOD.iss"
#include "src\customPages\itemsTweaker.iss"
#include "src\customPages\ready\ready.iss"
#include "src\customPages\preparing.iss"
#include "src\customPages\installing.iss"
#include "src\customPages\xvmDownload.iss"
#include "src\customPages\finished.iss"
#include "src\folderOperations.iss"

[Files]
Source: "files\console\*"; DestDir: "{app}\kmp"; Flags: ignoreversion;

[InstallDelete]
Type: filesandordirs; Name: "{app}\kmp";
Type: filesandordirs; Name: "{app}\win32\Reports_XFW";
Type: filesandordirs; Name: "{app}\replays\replays_manager";
Type: files; Name: "{app}\mods\configs\updater\updaterClient.json";
Type: files; Name: "{app}\*.log";

[UninstallDelete]
Type: filesandordirs; Name: "{app}\kmp";
Type: files; Name: "{userdesktop}\{cm:UninstallProgram,{#AppShortName}}";
Type: files; Name: "{app}\*.log";
Type: filesandordirs; Name: "{app}\win32\Reports_XFW";
Type: filesandordirs; Name: "{app}\mods\temp";
Type: filesandordirs; Name: "{app}\mods\configs";
Type: filesandordirs; Name: "{app}\mods\{#Patch}\com.modxvm.xfw";
Type: filesandordirs; Name: "{app}\res_mods\mods";
Type: filesandordirs; Name: "{app}\res_mods\configs";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\audioww";
Type: filesandordirs; Name: "{app}\res_mods\{#Patch}\scripts";
Type: filesandordirs; Name: "{app}\replays\replays_manager";

[Code]
Function InitializeSetup(): Boolean;
begin
 if ActiveLanguage() = 'en' then
  MsgBoxEx(0, CustomMessage('languageIsNotFullySupports'), SetupMessage(msgInformationTitle), MB_ICONINFORMATION or MB_OK, 0, 0);

 Result := True;
  #ifdef VCL
 InitializeVCL();
 #endif
end;

Procedure InitializeWizard();
begin
 if not CMDCheckParams(CMD_NoCheckForMutex) then
  CreateMutex('{#AppMutex}');
 InitializeWindow();
 InitializeWidgets();
 InitializeWelcomePage();
  #ifdef Updater
 InitializeUpdaterPage();
 #endif
 InitializeSelectDirPage();
 InitializeComponentsInfo();
 InitializeSounds();
 InitializeComponentsPage();
 InitializeXVMPage();
 InitializePMODPage();
 InitializeTweakerPage();
 InitializeReadyPage();
 InitializeInstallingPage();
 InitializeXVMDownloadPage();
 InitializeFinishedPage();
 //AnalyticsSessionStart();
end;

Function ShouldSkipPage(CurPageID: Integer): Boolean;
begin
 Result := False;
 case CurPageID of
  wpSelectDir: Result := SelectDirShouldSkipPage();
  wpPreparing: Result := PreparingShouldSkipPage();
 end;
end;

Procedure CurPageChanged(CurPageID: Integer);
begin
 case CurPageID of
  wpInstalling: ShowInstallingPage();
 end;
 ImgApplyChanges(WizardForm.Handle);
end;

Procedure CurStepChanged(CurStep: TSetupStep);
begin
 ClientFolderOperations(CurStep);
  #ifndef TESTING
 StartConfigurator(CurStep);
 #endif
 RememberComponentItems(CurStep);
 RememberPMODItems(CurStep);
 RememberTweakerItems(CurStep);
 RememberXVMItems(CurStep);
 SaveReadyMemoToLog(CurStep);
end;

Procedure DeinitializeSetup();
begin
 LaunchGame();
 //AnalyticsSessionFinish();
 DeinitializeSounds();
 gdipShutdown();
  #ifdef VCL
 UnLoadVCLStyles();
 #endif
 DelTree(ExpandConstant('{tmp}'), True, True, True);
end;

Function InitializeUninstall(): Boolean;
begin
 Result := True;
  #ifdef CheckForGameRun
 Result := CheckForGameRun(0);
 #endif
end;

Procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
 RestoreDirectories(CurUninstallStep);
end;