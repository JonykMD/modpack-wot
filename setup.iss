﻿// © Kotyarko_O, 2020 \\

[Setup]
AppId={{#AppID}
AppMutex={#AppMutex}
AppName={#AppFullName} ({#Version})
AppVersion={#GameShortName} ({#Patch})
AppPublisher={#Author}
AppPublisherURL={#URL_WOTForum}
AppSupportURL={#URL_WOTForum}
AppUpdatesURL={#URL_WOTForum}
DefaultDirName={pf}\World_of_Tanks
AppendDefaultDirName=no
DirExistsWarning=no
DefaultGroupName={#AppFullName} ({#Version})
DisableWelcomePage=yes
DisableProgramGroupPage=yes
DisableDirPage=yes
DisableReadyPage=yes
DisableFinishedPage=yes
OutputDir=BUILDED
OutputBaseFilename={#AppShortName}-{#Version}
SetupIconFile=files\logo.ico
AppComments={#Author}
VersionInfoVersion={#Version}
VersionInfoTextVersion={#Version}
VersionInfoDescription={#AppFullName} for {#GameFullName}
AppCopyright=© {#Author} 2015-2020
UninstallLogMode=new
UninstallDisplayIcon={app}\kmp\unins000.exe
UninstallFilesDir={app}\kmp
UninstallDisplayName={#AppFullName} ({#Version})
UsePreviousSetupType=no
PrivilegesRequired=poweruser
 #ifdef Compress
Compression=lzma2
SolidCompression=yes
LZMAUseSeparateProcess=yes
LZMADictionarySize=262144
LZMABlockSize=262144
LZMANumBlockThreads=1
#endif

[Languages]
Name: "en"; MessagesFile: "src\l10n\setup_en.isl";
Name: "ru"; MessagesFile: "src\l10n\setup_ru.isl";